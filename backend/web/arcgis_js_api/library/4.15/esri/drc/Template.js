define(
    [
        "esri/Graphic",
        "esri/symbols/PictureMarkerSymbol",
        "esri/symbols/TextSymbol"
    ],
    function (Graphic, PictureMarkerSymbol, TextSymbol) {
        const minScale = 2500000;

        function createOfficeTextSymbol(color) {
            return {
                type: "text", // autocasts as new TextSymbol()
                font: {
                    size: 10,
                },
                color: color
            };
        }

        function createPoleTextSymbol(color) {
            return {
                type: "text", // autocasts as new TextSymbol()
                font: {
                    size: 6,
                    weight: "normal"
                },
                color: color,
            };
        }

        function createOfficeTemplate() {
            return {
                // autocasts as new PopupTemplate()
                title: "{OFFICENAME}",
                content: [
                    {
                        // It is also possible to set the fieldInfos outside of the content
                        // directly in the popupTemplate. If no fieldInfos is specifically set
                        // in the content, it defaults to whatever may be set within the popupTemplate.
                        type: "fields",
                        fieldInfos: [
                            {
                                fieldName: "ADDRESS",
                                label: "ที่อยู่"
                            },
                            {
                                fieldName: "TELEPHONE",
                                label: "หมายเลขโทรศัพท์ ",
                            },
                            {
                                fieldName: "OFFICE_HOURS",
                                label: "เวลาทำการ",
                            },
                            {
                                fieldName: "LOCATION_DES",
                                label: "รายละเอียดเพิ่มเติม",
                            }
                        ]
                    }
                ]
            };

        }

        function createStationTemplate() {
            return {
                // autocasts as new PopupTemplate()
                title: "{NAME_THAI}",
                content: [
                    {
                        // It is also possible to set the fieldInfos outside of the content
                        // directly in the popupTemplate. If no fieldInfos is specifically set
                        // in the content, it defaults to whatever may be set within the popupTemplate.
                        type: "fields",
                        fieldInfos: [
                            {
                                fieldName: "SUBTYPECODE ",
                                label: "แรงดันขาเข้าของสถานีไฟฟ้า",
                            },
                            {
                                fieldName: "SECONDARYVOLTAGE ",
                                label: "แรงดันขาออก",
                            },
                            {
                                fieldName: "SUBSTATIONTYPE ",
                                label: "ประเภทของสถานีไฟฟ้า",
                            },
                            {
                                fieldName: "OUTAGEAREA",
                                label: "พื้นที่จ่ายไฟ ",
                            },
                            {
                                fieldName: "LOCATION  ",
                                label: "สถานที่",
                            },
                            {
                                fieldName: "COMMENTS ",
                                label: "หมายเหตุ",
                            },
                        ]
                    }
                ]
            };
        }

        function createSwitchTemplate() {
            return {
                // autocasts as new PopupTemplate()
                title: "{TAG}",
                content: [
                    {
                        // It is also possible to set the fieldInfos outside of the content
                        // directly in the popupTemplate. If no fieldInfos is specifically set
                        // in the content, it defaults to whatever may be set within the popupTemplate.
                        type: "fields",
                        fieldInfos: [
                            {
                                fieldName: "SUBTYPECODE ",
                                label: "ประเภทย่อยของสวิทช์",
                            },
                            {
                                fieldName: "OP_VOLT",
                                label: "ระดับแรงดัน",
                            },
                            {
                                fieldName: "PRESENTPOSITION",
                                label: " สถานะปัจจุบัน",
                            },
                            {
                                fieldName: "MAXINTERRUPTINGCURRENT ",
                                label: "ค่ากระแสลัดวงจรสูงสุด (kA)",
                            },
                            {
                                fieldName: "MAXCONTINUOUSCURRENT ",
                                label: "ค่ากระแสต่อเนื่องสูงสุด",
                            },
                            {
                                fieldName: "LOCATION",
                                label: "สถานที่",
                            },
                        ]
                    }
                ]
            };
        }

        function createPoleTemplate() {
            return {
                // autocasts as new PopupTemplate()
                title: "{TAG}",
                content: [
                    {
                        // It is also possible to set the fieldInfos outside of the content
                        // directly in the popupTemplate. If no fieldInfos is specifically set
                        // in the content, it defaults to whatever may be set within the popupTemplate.
                        type: "fields",
                        fieldInfos: [
                            {
                                fieldName: "SUBTYPECODE ",
                                label: "รหัสระดับแรงดันของเสา",
                            },
                            {
                                fieldName: "OP_VOLT",
                                label: "ระดับแรงดัน",
                            },
                            {
                                fieldName: "HEIGHT",
                                label: "ความสูงเสา",
                            },
                            {
                                fieldName: "STREETLIGHT ",
                                label: " ประเภทโคมไฟสาธารณะ",
                            },
                            {
                                fieldName: "LOCATION",
                                label: "สถานที่",
                            },
                            {
                                fieldName: "COMMENT1 ",
                                label: "หมายเหตุที่ 1",
                            },
                            {
                                fieldName: "COMMENT2",
                                label: "หมายเหตุที่ 2",
                            },
                        ]
                    }
                ]
            };
        }

        function createStationLabelInfo() {
            const StationArcade = "$feature.NAME_THAI ";
            const StationClass = {
                labelPlacement: "above-right",
                labelExpressionInfo: {
                    expression: StationArcade
                },
                minScale: minScale
            };
            StationClass.symbol = createOfficeTextSymbol("#404040");

            return StationClass;
        }

        function createOfficeLabelInfo() {
            const OfficeArcade = "$feature.OFFICENAME";
            const OfficeClass = {
                labelPlacement: "above-right",
                labelExpressionInfo: {
                    expression: OfficeArcade
                },
                minScale: minScale
            };
            OfficeClass.symbol = createOfficeTextSymbol("#404040");

            return OfficeClass;
        }

        function createPoleLabelInfo() {
            const PoleArcade = "$feature.OP_VOLT + $feature.SUBTYPECODE  ";
            const PoleClass = {
                labelPlacement: "above-right",
                labelExpressionInfo: {
                    expression: PoleArcade
                },
                minScale: minScale
            };
            PoleClass.symbol = createPoleTextSymbol("#828282");
            return PoleClass;
        }

        function createSwitchLabelInfo() {
            const SwitchArcade = "$feature.OP_VOLT + $feature.SUBTYPECODE";
            const SwitchClass = {
                labelPlacement: "above-right",
                labelExpressionInfo: {
                    expression: SwitchArcade
                },
                minScale: minScale
            };
            SwitchClass.symbol = createPoleTextSymbol("#404040");

            return SwitchClass;
        }

        function createBuildSymbol(color) {
            return {
                type: "point-3d", // autocasts as new PointSymbol3D()
                symbolLayers: [
                    {
                        type: "object", // autocasts as new ObjectSymbol3DLayer()
                        width: 20,
                        height: 30,
                        resource: {
                            primitive: "cube"
                        },
                        material: {
                            color: color
                        }
                    }
                ]
            };
        }

        function createPoleSymbol() {
            return {
                type: "point-3d",  // autocasts as new PointSymbol3D()
                symbolLayers: [{
                    type: "object",  // autocasts as new ObjectSymbol3DLayer()
                    width: 2.1050639748573303,
                    height: 6.192150592803955,
                    depth: 0.9093920290470123,
                    anchor: "origin",
                    resource: {
                        //href: "../img/firetruck.glb"
                        href: "https://static.arcgis.com/arcgis/styleItems/Infrastructure/web/resource/Powerline_Pole.json"
                    },
                    //material: { color: "red" }
                }]
            };
        }

        function createSwitchSymbol() {
            return {
                type: "point-3d",  // autocasts as new PointSymbol3D()
                symbolLayers: [{
                    type: "object",  // autocasts as new ObjectSymbol3DLayer()
                    width: 1.3974600434303284,
                    height: 2.309540033340454,
                    depth: 0.9093920290470123,
                    anchor: "origin",
                    resource: {
                        href: "https://static.arcgis.com/arcgis/styleItems/Infrastructure/web/resource/Electricity_Meter.json"
                    }
                }]
            };
        }

        function createFireTruckSymbol(point) {
            return new Graphic({
                geometry: point,
                symbol: {
                    type: "picture-marker",  // autocasts as new PictureMarkerSymbol()
                    url: "./img/firetruck.png",
                    width: "50px",
                    height: "50px"
                },

            });
        }

        function createCarSymbol(point) {
            return new Graphic({
                geometry: point,
                symbol: {
                    type: "picture-marker",  // autocasts as new PictureMarkerSymbol()
                    url: "./img/png-voiture-4.png",
                    width: "50px",
                    height: "50px"
                },

            });
        }

        function createTrackCarSymbol(point) {
            return new Graphic({
                geometry: point,
                symbol: {
                    type: "picture-marker",  // autocasts as new PictureMarkerSymbol()
                    url: "./img/car_red.png",
                    width: "50px",
                    height: "50px"
                },

            });
        }

        function createCar3DSymbol(point) {
            return new Graphic({
                geometry: point,
                symbol: {
                    type: "picture-marker",  // autocasts as new PictureMarkerSymbol()
                    url: "./img/png-voiture-4.png",
                    width: "50px",
                    height: "50px"
                },

            });
        }

        function createTextSymbol(point, text) {
            return new Graphic({
                geometry: point,
                symbol: {
                    type: "text",  // autocasts as new TextSymbol()
                    color: "white",
                    haloColor: "black",
                    haloSize: "1px",
                    text: text,
                    xoffset: 3,
                    yoffset: 30,
                    verticalAlignment: "top",
                    horizontalAlignment: "right",
                    lineHeight: 6,
                    font: {  // autocasts as new Font()
                        size: 8,
                        family: "Josefin Slab",
                        weight: "normal"
                    }
                }
            });
        }

        return {
            createOfficeTemplate: createOfficeTemplate,
            createStationTemplate: createStationTemplate,
            createSwitchTemplate: createSwitchTemplate,
            createPoleTemplate: createPoleTemplate,
            createStationLabelInfo: createStationLabelInfo,
            createOfficeLabelInfo: createOfficeLabelInfo,
            createPoleLabelInfo: createPoleLabelInfo,
            createSwitchLabelInfo: createSwitchLabelInfo,
            createBuildSymbol: createBuildSymbol,
            createPoleSymbol: createPoleSymbol,
            createSwitchSymbol: createSwitchSymbol,
            createCarSymbol: createCarSymbol,
            createTextSymbol: createTextSymbol,
            createCar3DSymbol: createCar3DSymbol,
            createTrackCarSymbol: createTrackCarSymbol,

        };

    }
);