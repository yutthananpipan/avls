define(
    [
        "esri/layers/FeatureLayer",
        "esri/widgets/Locate",
        "esri/Graphic",
        "esri/widgets/BasemapToggle",
        "esri/drc/Template",
        "esri/widgets/Search",
        "esri/widgets/Daylight",
        "esri/widgets/Expand",
        "esri/widgets/support/DatePicker",
        "geolocate", // geolocation simulator (https://github.com/2gis/mock-geolocation)
        "esri/widgets/Track",
        "esri/symbols/PictureMarkerSymbol",
        "esri/symbols/MarkerSymbol",
        "esri/layers/GraphicsLayer",

    ],
    function (FeatureLayer, Locate, Graphic, BasemapToggle, Template, Search, Daylight, Expand, DatePicker, geolocate, Track, PictureMarkerSymbol, MarkerSymbol, GraphicsLayer) {
        var data;
        var cars = [];
        const minimum_online_minute = 30;
        var track_uuid = '';

        var track_timeline;
        var track_car;
        var graphicsLayer;

        var coords = [];
        var timeline_symbols = [];

        var last_latitude = 0;
        var last_longitude = 0;

        class Car {
            constructor(name, car_symbol, cardesc_symbol, latitude, longitude) {
                this.name = name;
                this.car_symbol = car_symbol;
                this.cardesc_symbol = cardesc_symbol;
                this.lat = latitude;
                this.lng = longitude;
            }
        }

        function _View(v) {
            view = v;
        }

        function createLocal(view) {
            return new Locate({
                view: view,
                useHeadingEnabled: false,
                goToOverride: function (view, options) {
                    options.target.scale = 1500;  // Override the default map scale
                    return view.goTo(options.target);
                }
            });
        }

        function createToggleBaseMap(view) {
            return new BasemapToggle({
                // 2 - Set properties
                view: view, // view that provides access to the map's 'topo' basemap
                nextBasemap: "hybrid" // allows for toggling to the 'hybrid' basemap
            });
        }

        function createStationLayer() {
            return new FeatureLayer({
                url: "https://map.pea.co.th/arcgis/rest/services/Car_Tracking/MapServer/0",
                renderer: {
                    type: "simple",
                    symbol: Template.createBuildSymbol("#a4a4a4")
                },
                labelingInfo: [
                    Template.createStationLabelInfo(),
                ],
                popupTemplate: Template.createStationTemplate()
            });
        }

        function createPoleLayer() {
            return new FeatureLayer({
                url: "https://map.pea.co.th/arcgis/rest/services/Car_Tracking/MapServer/1",
                renderer: {
                    type: "simple",
                    symbol: Template.createPoleSymbol()
                },
                labelingInfo: [
                    Template.createPoleLabelInfo(),
                ],
                popupTemplate: Template.createPoleTemplate()
            });
        }

        function createSwitchLayer() {
            return new FeatureLayer({
                url: "https://map.pea.co.th/arcgis/rest/services/Car_Tracking/MapServer/2",
                renderer: {
                    type: "simple",
                    symbol: Template.createSwitchSymbol()
                },
                labelingInfo: [
                    Template.createSwitchLabelInfo(),
                ],
                popupTemplate: Template.createSwitchTemplate()
            });
        }

        function createOfficeLayer() {
            return new FeatureLayer({
                url: "https://map.pea.co.th/arcgis/rest/services/Car_Tracking/MapServer/3",
                renderer: {
                    type: "simple",
                    symbol: Template.createBuildSymbol("#a4a4a4")
                },
                labelingInfo: [
                    Template.createOfficeLabelInfo(),
                ],
                popupTemplate: Template.createOfficeTemplate()
            });
        }

        function createSearchLayer(view, station_layer, office_layer) {
            const searchWidget = new Search({
                view: view,
                allPlaceholder: "Office or Station",
                sources: [
                    {
                        layer: station_layer,
                        searchFields: ["STATIONNAME", "NAME_THAI", "OUTAGEAREA", "LOCATION"],
                        displayField: "NAME_THAI",
                        suggestionTemplate: "{NAME_THAI}",
                        exactMatch: false,
                        outFields: ["*"],
                        name: "PEA STATION",
                        placeholder: "สถานีไฟฟ้า",
                        zoomScale: 500000,
                        resultSymbol: {
                            type: "picture-marker", // autocasts as new PictureMarkerSymbol()
                            url:
                                "https://developers.arcgis.com/javascript/latest/sample-code/widgets-search-multiplesource/live/images/senate.png",
                            height: 36
                        }
                    },
                    {
                        layer: office_layer,
                        searchFields: ["OFFICENAME", "ADDRESS", "TELEPHONE", "LOCATION_DES"],
                        displayField: "OFFICENAME",
                        suggestionTemplate: "{OFFICENAME}",
                        exactMatch: false,
                        outFields: ["*"],
                        name: "PEA OFFICE",
                        placeholder: "สำนักงานการไฟฟ้า",
                        zoomScale: 500000,
                        resultSymbol: {
                            type: "picture-marker", // autocasts as new PictureMarkerSymbol()
                            url:
                                "https://developers.arcgis.com/javascript/latest/sample-code/widgets-search-multiplesource/live/images/senate.png",
                            height: 36
                        }
                    },
                ]
            });

            // add the daylight widget inside of Expand widget
            const expand = new Expand({
                expandIconClass: "esri-icon-search",
                expandTooltip: "Expand Search widget",
                view: view,
                content: searchWidget,
                expanded: false
            });

            view.ui.add(expand, "top-left");
        }

        function loadDeviceList(view) {
            // Todo button List of Device
            const listNode = document.getElementById("nyc_graphics");

            // Todo button Timeline
            const listDeviceNode = document.getElementById("DeviceList");
            const searchBtn = document.getElementById("searchTimeline");
            searchBtn.addEventListener('click', onSearchListening);

            // Todo button of track a car
            const listTrackNode = document.getElementById("TrackList");
            const trackBtn = document.getElementById("trackcarbtn");
            trackBtn.addEventListener('click', onTrackListening);

            $.get('?r=device/get-devices', function (res) {

                data = res;
                const fragment = document.createDocumentFragment();
                const TimelineFragment = document.createDocumentFragment();
                const TrackFragment = document.createDocumentFragment();

                Object.keys(res).forEach(function (index) {
                    console.log(res[index].name);
                    const attributes = res[index].attributes;
                    const name = res[index].name;
                    const last_on = res[index].last_on;
                    const latitude = res[index].lat;
                    const longitude = res[index].lng;

                    var startDate = new Date(1000 * last_on);
                    // Do your operations
                    var endDate = new Date();
                    var minutes = ((endDate.getTime() - startDate.getTime()) / 1000) / 60;

                    // Create a list zip codes in NY
                    const li = document.createElement("li");
                    li.classList.add("panel-result");
                    li.tabIndex = 0;
                    li.setAttribute("data-result-id", index);
                    li.setAttribute("id", name);

                    // List of timeline
                    const timeline_li = document.createElement("li");
                    timeline_li.classList.add("device_result");
                    timeline_li.tabIndex = 0;
                    timeline_li.style.color = "white";
                    timeline_li.setAttribute("timeline-id", index);
                    timeline_li.setAttribute("id", 'timeline' + name);

                    // List of track a car
                    const track_dropdown_li = document.createElement("li");
                    track_dropdown_li.classList.add("device_result");
                    track_dropdown_li.tabIndex = 0;
                    track_dropdown_li.style.color = "white";
                    track_dropdown_li.setAttribute("track-id", index);
                    track_dropdown_li.setAttribute("id", 'track' + name);

                    // Todo create Icon
                    const tagicon = document.createElement("i");
                    tagicon.setAttribute("id", "icon" + index);
                    tagicon.classList.add("fa");
                    tagicon.classList.add("fa-circle");
                    tagicon.style.color = (minutes < minimum_online_minute) ? "green" : "orange";
                    tagicon.setAttribute("data-result-id", index);
                    li.appendChild(tagicon);

                    // Todo create Device Name
                    const tagname = document.createElement("span");
                    tagname.style.marginLeft = "2px";
                    tagname.innerHTML = name;
                    tagname.setAttribute("data-result-id", index);
                    li.appendChild(tagname);

                    // add element for timeline
                    timeline_li.appendChild(tagname.cloneNode(true));
                    TimelineFragment.appendChild(timeline_li);

                    // add element for track
                    track_dropdown_li.appendChild(tagname.cloneNode(true));
                    TrackFragment.appendChild(track_dropdown_li);

                    // element for device list
                    fragment.appendChild(li);

                    // update view
                    UpdateDeviceLocation(view, name, latitude, longitude, last_on);

                });

                var cln = fragment.cloneNode(true);

                listNode.innerHTML = "";
                listNode.appendChild(fragment);

                listDeviceNode.innerHTML = "";
                listDeviceNode.appendChild(TimelineFragment);

                listTrackNode.innerHTML = "";
                listTrackNode.appendChild(TrackFragment);

            }, 'json').fail(function (e) {
                //document.getElementById("image4").src = arr[3].src;
            });

            // listen to click event on the zip code list
            listNode.addEventListener("click", onListClickHandler);
            listDeviceNode.addEventListener("click", onTimelineDeviceListClickHandler);
            listTrackNode.addEventListener("click", onTrackDeviceListClickHandler);

        }

        function onListClickHandler(event) {
            const target = event.target;
            const resultId = target.getAttribute("data-result-id");
            console.log(data[resultId]);

            ViewChangeListening(view, data[resultId].name);

        }

        function onTimelineDeviceListClickHandler(event) {
            const target = event.target;
            const resultId = target.getAttribute("data-result-id");
            if (resultId != null) {
                console.log(data[resultId]);

                const timeline_btn = document.getElementById("timeline_btn");
                timeline_btn.innerHTML = "";
                timeline_btn.innerHTML = data[resultId].name.toString();

                // Todo create Icon
                const tag_icon = document.createElement("i");
                tag_icon.style.marginLeft = "16px";
                tag_icon.style.cssFloat = "right";
                tag_icon.classList.add("esri-icon-down-arrow");
                timeline_btn.appendChild(tag_icon);
            }
        }

        function onTrackDeviceListClickHandler(event) {
            const target = event.target;
            const resultId = target.getAttribute("data-result-id");
            if (resultId != null) {
                console.log(data[resultId]);

                const btn = document.getElementById("track_dropdown_btn");
                btn.innerHTML = "";
                btn.innerHTML = target.innerText;

                // Todo create Icon
                const tag_icon = document.createElement("i");
                tag_icon.style.marginLeft = "16px";
                tag_icon.style.cssFloat = "right";
                tag_icon.classList.add("esri-icon-down-arrow");
                btn.appendChild(tag_icon);
            }
        }

        function onSearchListening(event) {
            try {
                let track_status = track_timeline.viewModel.tracking;
                if (track_status) {
                    coords = [];

                } else {
                    const timeline_btn = document.getElementById("timeline_btn");
                    const date_start = document.getElementById("date-picker_start");
                    const date_end = document.getElementById("date-picker_end");
                    let uuid = '';

                    // Todo Clear line and Maker symbol
                    for (let i = 0; i < timeline_symbols.length; i++) {
                        view.graphics.remove(timeline_symbols[i]);
                        graphicsLayer.remove(timeline_symbols[i]);
                    }

                    timeline_symbols = [];
                    // Todo End of Clear Marker

                    Object.keys(data).forEach(function (index) {
                        const n = timeline_btn.innerHTML.toString().search(data[index].name.toString().trim());
                        if (n >= 0) {
                            uuid = data[index].name;
                        }
                    });

                    let dt = new Date(date_start.firstChild.firstChild.innerHTML).getTime() / 1000;
                    let de = new Date(date_end.firstChild.firstChild.innerHTML + " 23:59:00").getTime() / 1000;
                    console.log('date_start : ' + dt);
                    console.log('date_end   : ' + de);

                    if (uuid === '') {
                        window.alert("กรุณาเลือกอหมายเลขุปกรณ์");
                    } else {
                        $.get('?r=device/get-timeline&name=' + uuid + '&date_start=' + dt + '&date_end=' + de, function (res) {
                            Object.keys(res).forEach(function (index) {
                                // Todo value from action
                                let name = res[index].name;
                                let created_at = res[index].created_at;
                                let latitude = res[index].lat;
                                let longitude = res[index].lng;

                                coords.push({lat: latitude, lng: longitude});
                            });

                            if (res.length === 0) {
                                window.alert("ไม่พบข้อมูล");
                            }
                        }, 'json').fail(function (e) {
                            //document.getElementById("image4").src = arr[3].src;
                        });
                    }
                }

            } catch (e) {
                console.log(e.toString());
            }
        }

        function onTrackListening(event) {

            const track_status = track_car.viewModel.tracking;

            if (track_status) {
                track_uuid = '';

            } else {
                let uuid = '';
                const track_dropdown_btn = document.getElementById("track_dropdown_btn");

                Object.keys(data).forEach(function (index) {
                    const n = track_dropdown_btn.innerHTML.toString().search(data[index].name.toString().trim());
                    if (n >= 0) {
                        uuid = data[index].name;
                    }
                });

                if (uuid === '') {
                    window.alert("กรุณาเลือกอหมายเลขุปกรณ์");
                } else {
                    track_uuid = uuid;
                    GetDevice(track_uuid);
                }
            }

        }

        function createCarSymbol(view, point) {
            // Todo Test add Device
            view.graphics.add(Template.createCarSymbol(point));
        }

        function UpdateDeviceLocation(view, name, latitude, longitude, last_on) {
            try {
                var startDate = new Date(1000 * last_on);
                // Do your operations
                var endDate = new Date();
                var minutes = ((endDate.getTime() - startDate.getTime()) / 1000) / 60;

                for (i = 0; i < cars.length; i++) {
                    const car = cars[i];
                    if (car.name.toString().trim() === name.toString().trim()) {
                        const obj = car.car_symbol;
                        const obj_desc = car.cardesc_symbol;

                        view.graphics.remove(obj);
                        view.graphics.remove(obj_desc);

                        cars.splice(i, 1);

                        if (track_uuid !== '' && track_uuid === name) {
                            geolocate.change({lat: latitude, lng: longitude});
                        }
                    }
                }

                if (minutes < minimum_online_minute) {
                    // Todo Test add Device
                    const point = {
                        type: "point", // autocasts as new Point()
                        longitude: longitude,
                        latitude: latitude
                    };

                    // Todo add view
                    const track_status = track_car.viewModel.tracking;

                    const car_symbol = (track_status) ? Template.createTrackCarSymbol(point) : Template.createCarSymbol(point);
                    const car_desc_symbol = Template.createTextSymbol(point, name + "\n" + "\n");
                    view.graphics.add(car_symbol);
                    view.graphics.add(car_desc_symbol);

                    const car = new Car(name, car_symbol, car_desc_symbol, latitude, longitude);
                    cars.push(car);

                }

            } catch (e) {
                console.log(e.toString());
            }
        }

        function UpdateDeviceStatus(name, last_on) {
            try {
                var startDate = new Date(1000 * last_on);
                // Do your operations
                var endDate = new Date();
                var minutes = ((endDate.getTime() - startDate.getTime()) / 1000) / 60;

                const icon = document.getElementById(name.toString().trim()).firstChild;
                icon.style.color = (minutes < minimum_online_minute) ? "green" : "orange";
            } catch (e) {

            }
        }

        function ViewChangeListening(view, name) {

            for (i = 0; i < cars.length; i++) {
                const car = cars[i];
                if (car.name.toString().trim() === name.toString().trim()) {
                    console.log('true');

                    try {
                        view.center = {
                            type: "point",
                            longitude: car.lng,
                            latitude: car.lat,
                        };
                        view.scale = 2500;
                        view.tilt = 50;

                    } catch (e) {
                        console.log(e.toString());
                    }

                }
            }
        }

        function createDataRequest(view, host, port) {
            setInterval(function () {
                try {
                    console.log(new Date());

                    IntervalDeviceData(view);

                    const track_status = track_car.viewModel.tracking;

                    if (track_status) {
                        if (track_uuid !== '') {
                            console.log('Get Device');
                            GetDevice(track_uuid);
                        }
                    }
                } catch (e) {

                }
            }, 60 * 1000);
        }

        function IntervalDeviceData(view) {
            $.get('?r=device/get-devices', function (res) {

                Object.keys(res).forEach(function (index) {
                    console.log(res[index].name);
                    const name = res[index].name;
                    const latitude = res[index].lat;
                    const longitude = res[index].lng;
                    const last_on = res[index].last_on;

                    try {
                        UpdateDeviceStatus(name, last_on);
                        UpdateDeviceLocation(view, name, latitude, longitude, last_on);
                    } catch (e) {
                        console.log(e.toString());
                    }
                });
            }, 'json').fail(function (e) {

            });
        }

        function GetDevice(name) {
            $.get('?r=device/get-device-gps' + '&uid=' + name, function (res) {
                console.log('get device ' + name + ' success');
            }, 'json').fail(function (e) {

            });
        }

        function createConnection(view, host, port) {
            try {
                var socket = io.connect(host + ':' + port, {
                    reconnection: true,
                    reconnectionDelay: 1000,
                    reconnectionDelayMax: 5000,
                    reconnectionAttempts: Infinity
                });

                // Event handler for new connections.
                // The callback function is invoked when a connection with the
                // server is established.
                socket.on('connect', function () {
                    console.log('connected');
                    socket.emit('new message', {data: 'I\'m connected!'});
                });

                socket.on('disconnect', function () {
                    console.log('disconnected');
                    window.setTimeout(this, 30000);
                });

                // Event handler for server sent data.
                // The callback function is invoked whenever the server emits data
                // to the client. The data is then displayed in the "Received"
                // section of the page.
                socket.on('new message', function (msg) {
                    console.log(msg.message);
                    try {
                        loadDeviceList(view);

                        const lat = msg.message.lat;
                        const lng = msg.message.lng;
                        if (lat > 0 && lng > 0) {
                            UpdateDeviceLocation(view, msg.message.device, lat, lng, msg.message.last_on);
                        }

                    } catch (e) {
                        console.log(e.toString());
                    }
                });
            } catch (e) {
                console.log(e.toString());
            }
        }

        function createSearchTimeline(view) {

            const contend = document.getElementById("timeline");

            const layerListExpand = new Expand({
                expandIconClass: "esri-icon-polyline",  // see https://developers.arcgis.com/javascript/latest/guide/esri-icon-font/
                // expandTooltip: "Expand LayerList", // optional, defaults to "Expand" for English locale
                view: view,
                content: contend,
            });
            view.ui.add(layerListExpand, "top-right");

            // typical usage
            var date_start = new DatePicker({
                id: "date_start",
                container: "date-picker_start", // DOM element for widget
                value: new Date(), // value that will initially display
            });

            var date_end = new DatePicker({
                id: "date_start",
                container: "date-picker_end", // DOM element for widget
                value: new Date(), // value that will initially display
            });

            contend.classList.add("show");

            TrackInterval();

            graphicsLayer = new GraphicsLayer();
            view.map.add(graphicsLayer);

            track_timeline = new Track({
                container: 'searchTimeline',
                view: view,
                graphic: new Graphic({
                    symbol: {
                        type: "picture-marker",  // autocasts as new PictureMarkerSymbol()
                        url: "./img/car_red.png",
                        width: "50px",
                        height: "50px"
                    },
                }),
                goToLocationEnabled: false // disable this since we want to control what happens after our location is acquired
            });

            view.when(function () {

                var prevLocation = view.center;

                track_timeline.on("track", function () {
                    var location = track_timeline.graphic.geometry;

                    let timeline_point = new Graphic({
                        geometry: {
                            type: "point", // autocasts as new Point()
                            longitude: location.longitude,
                            latitude: location.latitude,
                        },
                        symbol: {
                            type: "simple-marker",
                            color: [255, 61, 0, 1],
                            size: "8px",  // pixels
                            outline: {  // autocasts as new SimpleLineSymbol()
                                color: [255, 255, 255],
                                width: 1  // points
                            }
                        }
                    });

                    // add graphics location
                    view.graphics.add(timeline_point);
                    timeline_symbols.push(timeline_point);

                    if (last_latitude !== 0 && last_longitude !== 0) {
                        let polylineGraphic = new Graphic({
                            geometry: {
                                type: "polyline", // autocasts as new Polyline()
                                paths: [
                                    [last_longitude, last_latitude],
                                    [location.longitude, location.latitude],
                                ]
                            },
                            symbol: {
                                type: "simple-line", // autocasts as SimpleLineSymbol()
                                color: [226, 119, 40],
                                width: 2
                            },
                        });
                        graphicsLayer.add(polylineGraphic);
                        timeline_symbols.push(polylineGraphic);

                    }

                    view
                        .goTo({
                            center: location,
                            tilt: 50,
                            scale: 25000,
                            heading: 360 - getHeading(location, prevLocation), // only applies to SceneView
                            rotation: 360 - getHeading(location, prevLocation) // only applies to MapView
                        })
                        .catch(function (error) {
                            if (error.name != "AbortError") {
                                console.error(error);
                            }
                        });

                    prevLocation = location.clone();
                    last_latitude = location.latitude;
                    last_longitude = location.longitude;

                });

                track_timeline.stop();
            });
        }

        function getHeading(point, oldPoint) {
            // get angle between two points
            var angleInDegrees =
                (Math.atan2(point.y - oldPoint.y, point.x - oldPoint.x) * 180) /
                Math.PI;

            // move heading north
            return -90 + angleInDegrees;
        }

        function TrackInterval() {
            let currentCoordIndex = 0;
            geolocate.use();

            setInterval(function () {

                if (coords.length > 0) {
                    geolocate.change(coords[currentCoordIndex]);
                    currentCoordIndex = currentCoordIndex + 1;
                } else {
                    track_timeline.stop();
                    currentCoordIndex = 0;
                    last_latitude = 0;
                    last_longitude = 0;

                }
            }, 1000);
        }

        function createTrackCar(view) {
            const contend = document.getElementById("trackwidget");

            const layerListExpand = new Expand({
                expandIconClass: "esri-icon-tracking",  // see https://developers.arcgis.com/javascript/latest/guide/esri-icon-font/
                // expandTooltip: "Expand LayerList", // optional, defaults to "Expand" for English locale
                view: view,
                content: contend,
            });
            view.ui.add(layerListExpand, "top-right");

            contend.classList.add("show");

            track_car = new Track({
                view: view,
                container: 'trackcarbtn',
                graphic: new Graphic({
                    symbol: {
                        type: "picture-marker",  // autocasts as new PictureMarkerSymbol()
                        url: "./img/car_red.png",
                        width: "50px",
                        height: "50px"
                    },
                }),
                goToLocationEnabled: false // disable this since we want to control what happens after our location is acquired
            });

            view.when(function () {
                var prevLocation = view.center;

                track_car.on("track", function () {
                    var location_track = track_car.graphic.geometry;

                    view
                        .goTo({
                            center: location_track,
                            tilt: 50,
                            scale: 25000,
                            heading: 360 - getHeading(location_track, prevLocation), // only applies to SceneView
                            rotation: 360 - getHeading(location_track, prevLocation) // only applies to MapView
                        })
                        .catch(function (error) {

                        });

                });

                track_car.stop();
            });

        }

        return {
            createLocal: createLocal,
            createToggleBaseMap: createToggleBaseMap,
            createStationLayer: createStationLayer,
            createPoleLayer: createPoleLayer,
            createSwitchLayer: createSwitchLayer,
            createOfficeLayer: createOfficeLayer,
            createSearchLayer: createSearchLayer,
            getDeviceList: loadDeviceList,
            createCarSymbol: createCarSymbol,
            setDeviceLocation: UpdateDeviceLocation,
            setDeviceStatus: UpdateDeviceStatus,
            createDataRequest: createDataRequest,
            createConnection: createConnection,
            createSearchTimeline: createSearchTimeline,
            createTrackCar: createTrackCar,
            setView: _View,
        };

    }
);
