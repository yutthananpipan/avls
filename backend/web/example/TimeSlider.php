<html>
<head>
    <meta charset="utf-8" />
    <meta
        name="viewport"
        content="initial-scale=1,maximum-scale=1,user-scalable=no"
    />
    <!--
  ArcGIS API for JavaScript, https://js.arcgis.com
  For more information about the widgets-timeslider sample, read the original sample description at developers.arcgis.com.
  https://developers.arcgis.com/javascript/latest/sample-code/widgets-timeslider/index.html
  -->
    <title>TimeSlider widget - 4.15</title>

    <link
        rel="stylesheet"
        href="https://js.arcgis.com/4.15/esri/themes/light/main.css"
    />
    <script src="http://172.16.10.18/avls/backend/web/arcgis_js_api/library/4.15/dojo/dojo.js"></script>

    <style>
        html,
        body,
        #viewDiv {
            padding: 0;
            margin: 0;
            height: 100%;
            width: 100%;
        }

        #timeSlider {
            position: absolute;
            left: 100px;
            right: 100px;
            bottom: 20px;
        }

        #titleDiv {
            padding: 10px;
            font-weight: 36;
            text-align: center;
        }
    </style>
    <script>
        require([
            "esri/Map",
            "esri/views/MapView",
            "esri/layers/ImageryLayer",
            "esri/widgets/TimeSlider",
            "esri/widgets/Expand",
            "esri/widgets/Legend",
            "esri/widgets/support/DatePicker"
        ], function(Map, MapView, ImageryLayer, TimeSlider, Expand, Legend, DatePicker) {
            const layer = new ImageryLayer({
                url:
                    "https://sampleserver6.arcgisonline.com/arcgis/rest/services/ScientificData/SeaTemperature/ImageServer"
            });

            const map = new Map({
                basemap: "satellite",
                layers: [layer]
            });

            const view = new MapView({
                map: map,
                container: "viewDiv",
                zoom: 3,
                center: [-45, 30]
            });

            // typical usage
            var date = new DatePicker({
                container: "date-picker", // DOM element for widget
                value: "2019-12-25", // value that will initially display
            });

        });
    </script>
</head>

<body>
<div id="viewDiv"></div>
<div id="date-picker"></div>
<div id="titleDiv" class="esri-widget">
    <div id="titleText">Sea surface temperature</div>
</div>
</body>
</html>
