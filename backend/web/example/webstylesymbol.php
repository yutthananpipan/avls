<html>
<head>
    <meta charset="utf-8"/>
    <meta
            name="viewport"
            content="initial-scale=1,maximum-scale=1,user-scalable=no"
    />
    <!--
  ArcGIS API for JavaScript, https://js.arcgis.com
  For more information about the scene-shadow sample, read the original sample description at developers.arcgis.com.
  https://developers.arcgis.com/javascript/latest/sample-code/scene-shadow/index.html
  -->
    <title>SceneView - shadow and lighting settings - 4.15</title>
    <style>
        html,
        body,
        #viewDiv {
            padding: 0;
            margin: 0;
            height: 100%;
            width: 100%;
        }

        #environmentDiv {
            position: absolute;
            top: 12px;
            right: 12px;
            padding: 12px;
            background-color: rgba(0, 0, 0, 0.5);
            color: white;
        }
    </style>

    <link
            rel="stylesheet"
            href="https://js.arcgis.com/4.15/esri/themes/light/main.css"
    />
    <script src="http://172.16.10.18/avls/backend/web/arcgis_js_api/library/4.15/dojo/dojo.js"></script>

    <script>
        require([
            "esri/Graphic",
            "esri/views/SceneView",
            "esri/WebScene",
            "esri/symbols/WebStyleSymbol",
            "esri/layers/MapImageLayer",
            "esri/layers/GraphicsLayer",
            "esri/widgets/Sketch/SketchViewModel",
            "esri/layers/FeatureLayer",
            "esri/symbols/PictureMarkerSymbol"
        ], function (Graphic, SceneView, WebScene, WebStyleSymbol, MapImageLayer, GraphicsLayer, SketchViewModel, FeatureLayer, PictureMarkerSymbol) {
            const minScale = 2500000;

            // Create objectSymbol and add to renderer
            var buildingSymbol = {
                type: "point-3d", // autocasts as new PointSymbol3D()
                symbolLayers: [
                    {
                        type: "object", // autocasts as new ObjectSymbol3DLayer()
                        width: 20,
                        height: 30,
                        resource: {
                            primitive: "cube"
                        },
                        material: {
                            color: "#a4a4a4"
                        }
                    }
                ]
            };
/*

            const OfficeArcade = "$feature.OFFICENAME";
            const OfficeClass = {
                labelPlacement: "above-right",
                labelExpressionInfo: {
                    expression: OfficeArcade
                },
                minScale: minScale
            };
            OfficeClass.symbol = createOfficeTextSymbol("#404040");

            // symbol using a cylinder as a resource
            const buildingsymbol = {
                type: "point-3d", // autocasts as new PointSymbol3D()
                symbolLayers: [
                    {
                        type: "object", // autocasts as new ObjectSymbol3DLayer()
                        width: 20,
                        height: 30,
                        resource: {
                            primitive: "cube"
                        },
                        material: {
                            color: "#a4a4a4"
                        }
                    }
                ]
            };

            var stationlayer = new FeatureLayer({
                url: "https://map.pea.co.th/arcgis/rest/services/Car_Tracking/MapServer",
                renderer: {
                    type: "simple",
                    symbol: buildingSymbol
                },

            });

            var officelayer = new FeatureLayer({
                url: "https://map.pea.co.th/arcgis/rest/services/Car_Tracking/MapServer/3",
                renderer: {
                    type: "simple",
                    symbol: buildingsymbol
                },
                labelingInfo: [
                    OfficeClass,
                ],

            });

            // symbol using a cylinder as a resource
            const polsymbol = {
                type: "point-3d",  // autocasts as new PointSymbol3D()
                symbolLayers: [{
                    type: "object",  // autocasts as new ObjectSymbol3DLayer()
                    width: 2.1050639748573303,
                    height: 6.192150592803955,
                    depth: 0.9093920290470123,
                    anchor: "origin",
                    resource: {
                        //href: "../img/firetruck.glb"
                        href: "https://static.arcgis.com/arcgis/styleItems/Infrastructure/web/resource/Powerline_Pole.json"
                    },
                    //material: { color: "red" }
                }]
            };

            const PoleArcade = "$feature.OP_VOLT + $feature.SUBTYPECODE  ";
            const PoleClass = {
                labelPlacement: "above-right",
                labelExpressionInfo: {
                    expression: PoleArcade
                },
                minScale: minScale
            };
            PoleClass.symbol = createPOLeTextSymbol("#828282");

            const pole_layer = new FeatureLayer({
                url: "https://map.pea.co.th/arcgis/rest/services/Car_Tracking/MapServer/1",
                renderer: {
                    type: "simple",
                    symbol: polsymbol
                },
                labelingInfo: [
                    PoleClass,
                ],
            });
*/

            var scene = new WebScene({
                basemap: "streets-navigation-vector",
                ground: "world-elevation",
                //layers: [stationlayer, officelayer, pole_layer]
            });

            var point = {
                type: "point", // autocasts as new Point()
                longitude: 100.525608,
                latitude: 14.020153
            };

            var view = new SceneView({
                map: scene,
                scale: 50000,
                center: [100.525608, 14.020153],
                container: "viewDiv",
                environment: {
                    lighting: {
                        ambientOcclusionEnabled: true
                    }
                }
            });

            // Set the environment in SceneView
            view.environment = {
                lighting: {
                    directShadowsEnabled: true,
                    date: new Date("Sun Mar 15 2019 08:00:00 GMT+0100 (CET)")
                }
            };

            const cartrucksymbol = {
                type: "point-3d",  // autocasts as new PointSymbol3D()
                symbolLayers: [{
                    type: "object",  // autocasts as new ObjectSymbol3DLayer()
                    resource: {
                        href: "../img/firetruck.glb"
                    },
                    material: {color: "red"}
                }]
            };

            var picturesymbol = {
                type: "picture-marker",  // autocasts as new PictureMarkerSymbol()
                url: "../img/firetruck.png",
                width: "50px",
                height: "50px"
            };

            var cartruck = new Graphic({
                geometry: {
                    type: "point", // autocasts as new Point()
                    longitude: 100.525359,
                    latitude: 14.019582
                },
                symbol: picturesymbol,

            });

            view.graphics.add(cartruck);


            // Register the events to controls
            document
                .getElementById("timeOfDaySelect")
                .addEventListener("change", updateTimeOfDay);
            document
                .getElementById("directShadowsInput")
                .addEventListener("change", updateDirectShadows);
            document
                .getElementById("castShadowsInputBuidlings")
                .addEventListener("change", changeEventCastShadowBuidlings);
            document
                .getElementById("castShadowsInputZoningVolume")
                .addEventListener("change", changeEventCastShadowZoningVolume);
            document
                .getElementById("castShadowsInputCrane")
                .addEventListener("change", changeEventCastShadowCrane);

            view.when(function () {
                view.whenLayerView(view.map.layers.getItemAt(0)).then(function () {
                    updateCastShadowZoningVolume(false);
                });
                //createCraneGraphic();
                createCarGraphic();
                // createPoleGraphic();
                // createBuildingGraphic();
                //createFiretruckGraphic();
            });

            // Create the event's callback functions
            function updateTimeOfDay(ev) {
                var select = ev.target;
                var date = select.options[select.selectedIndex].value;
                view.environment.lighting.date = new Date(date);
            }

            function updateDirectShadows(ev) {
                view.environment.lighting.directShadowsEnabled = !!ev.target.checked;
            }

            function changeEventCastShadowBuidlings(ev) {
                var clone = view.map.layers.getItemAt(1).renderer.clone();
                clone.symbol.symbolLayers.getItemAt(0).castShadows = !!ev.target
                    .checked;
                view.map.layers.getItemAt(1).renderer = clone;
            }

            function changeEventCastShadowZoningVolume(ev) {
                updateCastShadowZoningVolume(!!ev.target.checked);
            }

            function updateCastShadowZoningVolume(castShadow) {
                var clone = view.map.layers.getItemAt(0).renderer.clone();
                clone.symbol.symbolLayers.getItemAt(0).castShadows = castShadow;
                view.map.layers.getItemAt(0).renderer = clone;
            }

            function changeEventCastShadowCrane(ev) {
                updateCastShadowCrane(!!ev.target.checked);
            }

            function updateCastShadowCrane(castShadow) {
                var clone = view.graphics.getItemAt(0).symbol.clone();
                clone.symbolLayers.getItemAt(0).castShadows = castShadow;
                view.graphics.getItemAt(0).symbol = clone;
            }

            function createCraneGraphic() {
                var craneSymbol = new WebStyleSymbol({
                    styleName: "EsriRealisticTransportationStyle",
                    name: "Tower_Crane"
                });
                craneSymbol.fetchSymbol().then(function (pointSymbol3D) {
                    var crane = new Graphic({
                        geometry: point,
                        symbol: pointSymbol3D
                    });
                    view.graphics.add(crane);
                    updateCastShadowCrane(false);
                });
            }

            function createCarGraphic() {
                var webStyleSymbol = new WebStyleSymbol({
                    name: "Ford_Transit_Connect",
                    styleName: "EsriRealisticTransportationStyle"
                });

                webStyleSymbol.fetchSymbol().then(function (pointSymbol3D) {
                    var car = new Graphic({
                        geometry: {
                            type: "point", // autocasts as new Point()
                            longitude: 100.525921,
                            latitude: 14.019337
                        },
                        symbol: pointSymbol3D
                    });

                    var symLyr = car.symbol.clone();

                    view.graphics.add(symLyr);
                });
            }

            function createPoleGraphic() {
                var webStyleSymbol = new WebStyleSymbol({
                    name: "Powerline_Pole",
                    styleName: "EsriInfrastructureStyle"
                });

                webStyleSymbol.fetchSymbol().then(function (pointSymbol3D) {
                    var pole1 = new Graphic({
                        geometry: {
                            type: "point", // autocasts as new Point()
                            longitude: 100.525653,
                            latitude: 14.020160
                        },
                        symbol: pointSymbol3D
                    });
                    var pole2 = new Graphic({
                        geometry: {
                            type: "point", // autocasts as new Point()
                            longitude: 100.525665,
                            latitude: 14.020278
                        },
                        symbol: pointSymbol3D
                    });
                    view.graphics.add(pole1);
                    view.graphics.add(pole2);

                });
            }

            function createBuildingGraphic() {
                var webStyleSymbol = new WebStyleSymbol({
                    name: "Industrial Complex",
                    styleName: "EsriIconsStyle"
                });

                webStyleSymbol.fetchSymbol().then(function (pointSymbol3D) {
                    var building = new Graphic({
                        geometry: {
                            type: "point", // autocasts as new Point()
                            longitude: 100.525955,
                            latitude: 14.020081
                        },
                        symbol: pointSymbol3D
                    });

                    view.graphics.add(building);

                });
            }

            function createFiretruckGraphic() {
                var webStyleSymbol = new WebStyleSymbol({
                    name: "Bus",
                    styleName: "EsriIconsStyle"
                });

                webStyleSymbol.fetchSymbol().then(function (pointSymbol3D) {
                    var building = new Graphic({
                        geometry: {
                            type: "point", // autocasts as new Point()
                            longitude: 100.525359,
                            latitude: 14.019582
                        },
                        symbol: pointSymbol3D
                    });

                    view.graphics.add(building);

                });
            }

            function createPOLeTextSymbol(color) {
                return {
                    type: "text", // autocasts as new TextSymbol()
                    font: {
                        size: 6,
                        weight: "normal"
                    },
                    color: color,
                };
            }

            function createOfficeTextSymbol(color) {
                return {
                    type: "text", // autocasts as new TextSymbol()
                    font: {
                        size: 10,
                    },
                    color: color
                };
            }
        });
    </script>
</head>
<body>
<div id="viewDiv"></div>
<div id="environmentDiv" class="esri-widget">
    <table>
        <tr>
            <td>Time of day:</td>
            <td>
                <select id="timeOfDaySelect" class="esri-widget">
                    <option value="Sun Mar 15 2019 09:00:00 GMT+0100 (CET)"
                    >Morning
                    </option
                    >
                    <option value="Sun Mar 15 2019 12:00:00 GMT+0100 (CET)"
                    >Noon
                    </option
                    >
                    <option value="Sun Mar 15 2019 16:00:00 GMT+0100 (CET)" selected
                    >Afternoon
                    </option
                    >
                    <option value="Sun Mar 15 2019 18:00:00 GMT+0100 (CET)"
                    >Evening
                    </option
                    >
                </select>
            </td>
        </tr>
        <tr>
            <td>Direct shadows general:</td>
            <td><input id="directShadowsInput" type="checkbox" checked/></td>
        </tr>
        <tr>
            <td><br/></td>
        </tr>
        <tr>
            <td><b>Change shadow on objects basis:</b></td>
        </tr>
        <tr></tr>
        <tr>
            <td>Buildings shadow:</td>
            <td>
                <input id="castShadowsInputBuidlings" type="checkbox" checked/>
            </td>
        </tr>
        <tr>
            <td>Zoning volume shadows:</td>
            <td><input id="castShadowsInputZoningVolume" type="checkbox"/></td>
        </tr>
        <tr>
            <td>Crane shadows:</td>
            <td><input id="castShadowsInputCrane" type="checkbox"/></td>
        </tr>
    </table>
</div>
</body>
</html>
