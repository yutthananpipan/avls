<html>
<head>
    <meta charset="utf-8"/>
    <meta
            name="viewport"
            content="initial-scale=1,maximum-scale=1,user-scalable=no"
    />
    <!--
  ArcGIS API for JavaScript, https://js.arcgis.com
  For more information about the widgets-track sample, read the original sample description at developers.arcgis.com.
  https://developers.arcgis.com/javascript/latest/sample-code/widgets-track/index.html
  -->
    <title>Track widget simulation - 4.15</title>

    <style>
        html,
        body,
        #viewDiv {
            padding: 0;
            margin: 0;
            height: 100%;
        }
    </style>
    <link
            rel="stylesheet"
            href="https://js.arcgis.com/4.15/esri/themes/light/main.css"
    />

    <script>
        var dojoConfig = {
            packages: [
                {
                    name: "geolocate",
                    location: "//2gis.github.io/mock-geolocation/dist",
                    main: "geolocate"
                }
            ]
        };
    </script>

    <script src="http://172.16.10.18/avls/backend/web/arcgis_js_api/library/4.15/dojo/dojo.js"></script>

    <script>
        require([
            "esri/Graphic",
            "geolocate", // geolocation simulator (https://github.com/2gis/mock-geolocation)
            "esri/widgets/Track",
            "esri/views/SceneView",
            "esri/Map",
            "esri/symbols/PictureMarkerSymbol",
            "esri/symbols/MarkerSymbol",
            "esri/layers/GraphicsLayer",

        ], function (Graphic, geolocate, Track, SceneView, Map, PictureMarkerSymbol, MarkerSymbol, GraphicsLayer) {
            // geolocation simulator
            stubGeolocation();

            var last_latitude = 0;
            var last_longitude = 0;


            var map = new Map({
                basemap: "topo"
            });

            var view = new SceneView({
                map: map,
                container: "viewDiv",
                center: [-117.187038, 34.057322],
                zoom: 18,
                ui: {
                    components: ["attribution"] // replace default set of UI components
                }
            });

            var graphicsLayer = new GraphicsLayer();
            map.add(graphicsLayer);

            var track = new Track({
                id: 'trackID',
                label: 'track',
                view: view,
                graphic: new Graphic({
                    symbol: {
                        type: "picture-marker",  // autocasts as new PictureMarkerSymbol()
                        url: "http://localhost/avls/backend/web/img/car_red.png",
                        width: "50px",
                        height: "50px"
                    },
                }),

                goToLocationEnabled: false // disable this since we want to control what happens after our location is acquired
            });

            view.ui.add(track, "top-left");

            view.when(function () {

                var prevLocation = view.center;

                track.on("track", function (trackEvent) {
                    var location = track.graphic.geometry;

                    let simple_point = new Graphic({
                        geometry: {
                            type: "point", // autocasts as new Point()
                            longitude: location.longitude,
                            latitude: location.latitude,
                        },
                        symbol: {
                            type: "simple-marker",
                            color: [255, 61, 0, 1],
                            size: "8px",  // pixels
                            outline: {  // autocasts as new SimpleLineSymbol()
                                color: [255, 255, 255],
                                width: 1  // points
                            }
                        }
                    });

                    // add graphics location
                    view.graphics.add(simple_point);

                    if (last_latitude !== 0 && last_longitude !== 0) {
                        let polylineGraphic = new Graphic({
                            geometry: {
                                type: "polyline", // autocasts as new Polyline()
                                paths: [
                                    [last_longitude, last_latitude],
                                    [location.longitude, location.latitude],
                                ]
                            },
                            symbol: {
                                type: "simple-line", // autocasts as SimpleLineSymbol()
                                color: [226, 119, 40],
                                width: 2
                            },
                        });
                        graphicsLayer.add(polylineGraphic);

                    }

                    view
                        .goTo({
                            center: location,
                            tilt: 50,
                            scale: 2500,
                            heading: 360 - getHeading(location, prevLocation), // only applies to SceneView
                            rotation: 360 - getHeading(location, prevLocation) // only applies to MapView
                        })
                        .catch(function (error) {
                            if (error.name != "AbortError") {
                                console.error(error);
                            }
                        });

                    prevLocation = location.clone();
                    last_latitude = location.latitude;
                    last_longitude = location.longitude;

                });

                track.emit("track", onListening);
                track.stop();
            });

            function onListening(event) {
                console.log(event.toString());
            }

            function createSwitchSymbol() {
                return {
                    type: "point-3d",  // autocasts as new PointSymbol3D()
                    symbolLayers: [{
                        type: "object",  // autocasts as new ObjectSymbol3DLayer()
                        width: 1.3974600434303284,
                        height: 2.309540033340454,
                        depth: 0.9093920290470123,
                        anchor: "origin",
                        resource: {
                            href: "https://static.arcgis.com/arcgis/styleItems/Infrastructure/web/resource/Electricity_Meter.json"
                        }
                    }]
                };
            }

            function getHeading(point, oldPoint) {
                // get angle between two points
                var angleInDegrees =
                    (Math.atan2(point.y - oldPoint.y, point.x - oldPoint.x) * 180) /
                    Math.PI;

                // move heading north
                return -90 + angleInDegrees;
            }

            // geolocation simulator
            function stubGeolocation() {
                var coords = [
                        {
                            lat: 34.05648363780692,
                            lng: -117.19565501782613
                        },
                        {
                            lng: -117.19565880345007,
                            lat: 34.05682230352545
                        },
                        {
                            lng: -117.19566258907402,
                            lat: 34.05716096924398
                        },
                        {
                            lng: -117.19566637469796,
                            lat: 34.05749963496251
                        },
                        {
                            lng: -117.19567016032191,
                            lat: 34.05783830068104
                        },
                        {
                            lng: -117.19567394594586,
                            lat: 34.05817696639957
                        },
                        {
                            lng: -117.1956777315698,
                            lat: 34.0585156321181
                        },
                        {
                            lng: -117.19568151719375,
                            lat: 34.05885429783663
                        },
                        {
                            lng: -117.1956853028177,
                            lat: 34.05919296355516
                        },
                        {
                            lat: 34.059192963555134,
                            lng: -117.19568530281771
                        },
                        {
                            lat: 34.05920092649827,
                            lng: -117.19575894615099
                        },
                        {
                            lng: -117.19575574232981,
                            lat: 34.058861053180614
                        },
                        {
                            lng: -117.19575253850863,
                            lat: 34.05852117986296
                        },
                        {
                            lng: -117.19574933468745,
                            lat: 34.0581813065453
                        },
                        {
                            lng: -117.19574613086627,
                            lat: 34.057841433227644
                        },
                        {
                            lng: -117.19574292704509,
                            lat: 34.05750155990999
                        },
                        {
                            lng: -117.19573972322391,
                            lat: 34.05716168659233
                        },
                        {
                            lng: -117.19573651940273,
                            lat: 34.056821813274674
                        },
                        {
                            lng: -117.19573331558155,
                            lat: 34.05648193995702
                        },
                        {
                            lat: 34.05648193995701,
                            lng: -117.19573331558153
                        },
                        {
                            lng: -117.19569416670383,
                            lat: 34.056482788881965
                        },
                        {
                            lng: -117.19565501782613,
                            lat: 34.05648363780692
                        }
                    ],
                    currentCoordIndex = 0;

                geolocate.use();

                setInterval(function () {
                    let b = track.viewModel.tracking;

                    geolocate.change(coords[currentCoordIndex]);
                    currentCoordIndex = (currentCoordIndex + 1) % coords.length;
                }, 1500);
            }
        });
    </script>
</head>

<body>
<div id="viewDiv"></div>
</body>
</html>
