var username = 'flex';
// var socket = io('http://localhost:8018');
namespace = '/test';
var URL_SERVER = 'https://localhost:443';
var socket = io.connect(URL_SERVER);

// Event handler for new connections.
// The callback function is invoked when a connection with the
// server is established.
socket.on('connect', function () {
    console.log('connected');
    socket.emit('new message', {data: 'I\'m connected!'});
});

// Event handler for server sent data.
// The callback function is invoked whenever the server emits data
// to the client. The data is then displayed in the "Received"
// section of the page.
socket.on('new message', function (msg) {
    console.log(msg)
});