require([
    "esri/Map",
    "esri/views/MapView",
    "esri/Graphic",
    "esri/views/SceneView",
    "esri/WebScene",
    "esri/layers/FeatureLayer",
    "esri/Color",
    "esri/widgets/Locate",
    "esri/widgets/BasemapToggle",
    "esri/widgets/Search",
    "esri/widgets/Daylight",
    "esri/widgets/Expand",
    "esri/widgets/support/DatePicker",
    "geolocate", // geolocation simulator (https://github.com/2gis/mock-geolocation)
    "esri/widgets/Track",
    "esri/symbols/PictureMarkerSymbol",
    "esri/symbols/MarkerSymbol",
    "esri/layers/GraphicsLayer",
    "esri/symbols/TextSymbol"
], function (
    Map, MapView, Graphic, SceneView, WebScene,
    FeatureLayer, Color, Locate, BasemapToggle,
    Search, Daylight, Expand, DatePicker, geolocate,
    Track, PictureMarkerSymbol, MarkerSymbol, GraphicsLayer, TextSymbol
) {

    const host = 'https://10.24.195.13';
    const socket_port = 8019;
    const api_port = 8018;

    let data;
    let cars = [];
    const minimum_online_minute = 30;
    let track_uuid = '';
    let track_uuid_focus = '';

    let track_timeline;
    let track_car;
    let graphicsLayer;

    let coords = [];
    let timeline_symbols = [];

    let last_latitude = 0;
    let last_longitude = 0;
    let online_color = "#2fff00";
    let offline_color = "#f40";

    let devicesAutoRequest = [];

    class Device {
        count_interval = 0;

        constructor(name, monitor, update_interval) {
            this.name = name;
            this.monitor = monitor;
            this.update_interval = update_interval;
        }
    }

    class Car {
        constructor(name, car_symbol, cardesc_symbol, latitude, longitude) {
            this.name = name;
            this.car_symbol = car_symbol;
            this.cardesc_symbol = cardesc_symbol;
            this.lat = latitude;
            this.lng = longitude;
        }
    }

    var scene = new WebScene({
        basemap: "streets-navigation-vector",
        ground: "world-elevation",

    });
    var view = new SceneView({
        container: "map",
        map: scene,
        padding: {
            top: 50,
            right: 250,
        },
        scale: 50000,
        center: [100.991555, 13.340582],
        ui: {
            components: ["attribution"] // replace default set of UI components
        }
    });

    document.getElementById("map").addEventListener("click", onListClickHandler);
    document.getElementById("panel-menu").addEventListener("click", onListClickHandler);
    document.getElementById("w0-collapse").addEventListener("click", onListClickHandler);

    // Todo Add Local Button
    view.ui.add(createLocal(), "top-left");

    // Todo add Togglebasemap
    view.ui.add(createToggleBaseMap(), "bottom-left");

    // Todo Add Device
    getDeviceList();
    createDataRequest(host, api_port);

    // Todo add service
    createConnection(host, socket_port);

    // Todo add Search Timeline Listening
    createSearchTimeline();

    // Todo add Track widget
    createTrackCar();

    function createLocal() {
        return new Locate({
            view: view,
            useHeadingEnabled: false,
            goToOverride: function (view, options) {
                options.target.scale = 1500;  // Override the default map scale
                return view.goTo(options.target);
            }
        });
    }

    function createToggleBaseMap() {
        return new BasemapToggle({
            // 2 - Set properties
            view: view, // view that provides access to the map's 'topo' basemap
            nextBasemap: "hybrid" // allows for toggling to the 'hybrid' basemap
        });
    }

    function getDeviceList() {
        // Todo button List of Device
        const listNode = document.getElementById("nyc_graphics");

        // Todo button Timeline
        const listDeviceNode = document.getElementById("DeviceList");
        const searchBtn = document.getElementById("searchTimeline");
        searchBtn.addEventListener('click', onSearchListening);

        // Todo button of track a car
        const listTrackNode = document.getElementById("TrackList");
        const trackBtn = document.getElementById("trackcarbtn");
        trackBtn.addEventListener('click', onTrackListening);

        $.get('?r=device/get-devices', function (res) {

            data = res;
            const fragment = document.createDocumentFragment();
            const TimelineFragment = document.createDocumentFragment();
            const TrackFragment = document.createDocumentFragment();

            Object.keys(res).forEach(function (index) {
                console.log(res[index].name);
                const name = res[index].name;
                const last_on = res[index].last_on;
                const latitude = res[index].lat;
                const longitude = res[index].lng;
                const monitor = res[index].monitor;
                const update_interval = res[index].update_interval;

                if (monitor > 0) {
                    const device = new Device(name, monitor, update_interval);
                    devicesAutoRequest.push(device);

                }

                var startDate = new Date(1000 * last_on);
                // Do your operations
                var endDate = new Date();
                var minutes = ((endDate.getTime() - startDate.getTime()) / 1000) / 60;

                // Create a list zip codes in NY
                const li = document.createElement("li");
                li.classList.add("panel-result");
                li.tabIndex = 0;
                li.setAttribute("data-result-id", index);
                li.setAttribute("id", name);

                // List of timeline
                const timeline_li = document.createElement("li");
                timeline_li.classList.add("device_result");
                timeline_li.tabIndex = 0;
                timeline_li.style.color = "white";
                timeline_li.setAttribute("timeline-id", index);
                timeline_li.setAttribute("id", 'timeline' + name);

                // List of track a car
                const track_dropdown_li = document.createElement("li");
                track_dropdown_li.classList.add("device_result");
                track_dropdown_li.tabIndex = 0;
                track_dropdown_li.style.color = "white";
                track_dropdown_li.setAttribute("track-id", index);
                track_dropdown_li.setAttribute("id", 'track' + name);

                // Todo create Icon
                const tagicon = document.createElement("i");
                tagicon.setAttribute("id", "icon" + index);
                tagicon.classList.add("fa");
                tagicon.classList.add("fa-circle");
                tagicon.style.color = (minutes < minimum_online_minute) ? online_color : offline_color;
                tagicon.setAttribute("data-result-id", index);
                li.appendChild(tagicon);

                // Todo create Device Name
                const tagname = document.createElement("span");
                tagname.style.marginLeft = "2px";
                tagname.innerHTML = name;
                tagname.setAttribute("data-result-id", index);
                li.appendChild(tagname);

                // add element for timeline
                timeline_li.appendChild(tagname.cloneNode(true));
                TimelineFragment.appendChild(timeline_li);

                // add element for track
                track_dropdown_li.appendChild(tagname.cloneNode(true));
                TrackFragment.appendChild(track_dropdown_li);

                // element for device list
                fragment.appendChild(li);

                // update view
                UpdateDeviceLocation(name, latitude, longitude, last_on);

            });

            listNode.innerHTML = "";
            listNode.appendChild(fragment);

            listDeviceNode.innerHTML = "";
            listDeviceNode.appendChild(TimelineFragment);

            listTrackNode.innerHTML = "";
            listTrackNode.appendChild(TrackFragment);

        }, 'json').fail(function (e) {
        });

        // listen to click event on the zip code list
        listNode.addEventListener("click", onListClickHandler);
        listDeviceNode.addEventListener("click", onTimelineDeviceListClickHandler);
        listTrackNode.addEventListener("click", onTrackDeviceListClickHandler);

    }

    function onListClickHandler(event) {
        try {

            const target = event.target;
            const resultId = target.getAttribute("data-result-id");
            console.log(data[resultId]);
            track_uuid_focus = data[resultId].name;

            ViewChangeListening(data[resultId].name);
            GetGPS(data[resultId].name);

        } catch (e) {
            console.log(e.toString());
            track_uuid_focus = '';
        }

    }

    function onTimelineDeviceListClickHandler(event) {
        const target = event.target;
        const resultId = target.getAttribute("data-result-id");
        const timeline_id = target.getAttribute("timeline-id");

        if (resultId != null) {
            console.log(data[resultId]);

            const timeline_btn = document.getElementById("timeline_btn");
            timeline_btn.innerHTML = "";
            timeline_btn.innerHTML = data[resultId].name.toString();

            // Todo create Icon
            const tag_icon = document.createElement("i");
            tag_icon.style.marginLeft = "16px";
            tag_icon.style.cssFloat = "right";
            tag_icon.classList.add("esri-icon-down-arrow");
            timeline_btn.appendChild(tag_icon);
        }else if(timeline_id != null){
            console.log(data[timeline_id]);

            const timeline_btn = document.getElementById("timeline_btn");
            timeline_btn.innerHTML = "";
            timeline_btn.innerHTML = data[timeline_id].name.toString();

            // Todo create Icon
            const tag_icon = document.createElement("i");
            tag_icon.style.marginLeft = "16px";
            tag_icon.style.cssFloat = "right";
            tag_icon.classList.add("esri-icon-down-arrow");
            timeline_btn.appendChild(tag_icon);
        }
    }

    function onTrackDeviceListClickHandler(event) {
        const target = event.target;
        const resultId = target.getAttribute("data-result-id");
        const track_id = target.getAttribute("track-id");

        if (resultId != null) {
            console.log(data[resultId]);

            const btn = document.getElementById("track_dropdown_btn");
            btn.innerHTML = "";
            btn.innerHTML = target.innerText;

            // Todo create Icon
            const tag_icon = document.createElement("i");
            tag_icon.style.marginLeft = "16px";
            tag_icon.style.cssFloat = "right";
            tag_icon.classList.add("esri-icon-down-arrow");
            btn.appendChild(tag_icon);
        }else if(track_id != null){
            console.log(data[track_id]);

            const btn = document.getElementById("track_dropdown_btn");
            btn.innerHTML = "";
            btn.innerHTML = target.innerText;

            // Todo create Icon
            const tag_icon = document.createElement("i");
            tag_icon.style.marginLeft = "16px";
            tag_icon.style.cssFloat = "right";
            tag_icon.classList.add("esri-icon-down-arrow");
            btn.appendChild(tag_icon);
        }
    }

    function onSearchListening(event) {
        try {
            let track_status = track_timeline.viewModel.tracking;
            if (track_status) {
                coords = [];
                document.getElementById("date_label").innerHTML = "";

            } else {
                const timeline_btn = document.getElementById("timeline_btn");
                const date_start = document.getElementById("date-picker_start");
                const date_end = document.getElementById("date-picker_end");
                let uuid = '';

                // Todo Clear line and Maker symbol
                for (let i = 0; i < timeline_symbols.length; i++) {
                    view.graphics.remove(timeline_symbols[i]);
                    graphicsLayer.remove(timeline_symbols[i]);
                }

                timeline_symbols = [];
                // Todo End of Clear Marker

                Object.keys(data).forEach(function (index) {
                    const n = timeline_btn.innerHTML.toString().search(data[index].name.toString().trim());
                    if (n >= 0) {
                        uuid = data[index].name;
                    }
                });

                let dt = new Date(date_start.firstChild.firstChild.innerHTML).getTime() / 1000;
                let de = new Date(date_end.firstChild.firstChild.innerHTML + " 23:59:00").getTime() / 1000;
                console.log('date_start : ' + dt);
                console.log('date_end   : ' + de);

                if (uuid === '') {
                    window.alert("กรุณาเลือกอหมายเลขุปกรณ์");
                } else {
                    $.get('?r=device/get-timeline&name=' + uuid + '&date_start=' + dt + '&date_end=' + de, function (res) {
                        Object.keys(res).forEach(function (index) {
                            // Todo value from action
                            let name = res[index].name;
                            let created_at = res[index].created_at;
                            let latitude = res[index].lat;
                            let longitude = res[index].lng;

                            coords.push({name: name, lat: latitude, lng: longitude, created_at: created_at});
                        });

                        if (res.length === 0) {
                            window.alert("ไม่พบข้อมูล");
                        }
                    }, 'json').fail(function (e) {
                        //document.getElementById("image4").src = arr[3].src;
                    });
                }
            }

        } catch (e) {
            console.log(e.toString());
        }
    }

    function onTrackListening(event) {

        const track_status = track_car.viewModel.tracking;

        if (track_status) {
            track_uuid = '';

        } else {
            let uuid = '';
            const track_dropdown_btn = document.getElementById("track_dropdown_btn");

            Object.keys(data).forEach(function (index) {
                const n = track_dropdown_btn.innerHTML.toString().search(data[index].name.toString().trim());
                if (n >= 0) {
                    uuid = data[index].name;
                }
            });

            if (uuid === '') {
                window.alert("กรุณาเลือกอหมายเลขุปกรณ์");
            } else {
                track_uuid = uuid;
                GetGPS(track_uuid);
            }
        }

    }

    function UpdateDeviceLocation(name, latitude, longitude, last_on) {
        try {
            var startDate = new Date(1000 * last_on);
            // Do your operations
            var endDate = new Date();
            var minutes = ((endDate.getTime() - startDate.getTime()) / 1000) / 60;

            for (i = 0; i < cars.length; i++) {
                const car = cars[i];
                if (car.name.toString().trim() === name.toString().trim()) {
                    const obj = car.car_symbol;
                    const obj_desc = car.cardesc_symbol;

                    view.graphics.remove(obj);
                    view.graphics.remove(obj_desc);

                    cars.splice(i, 1);

                    if (track_uuid !== '' && track_uuid === name) {
                        geolocate.change({lat: latitude, lng: longitude});

                    }
                }
            }

            // Todo Test add Device
            const point = {
                type: "point", // autocasts as new Point()
                longitude: longitude,
                latitude: latitude
            };

            // Todo add view
            const track_status = track_car.viewModel.tracking;
            let car_symbol = null;
            if (track_status && track_uuid === name) {
                car_symbol = createTrackCarSymbol(point);
            } else {
                car_symbol = createCarSymbol(point, (minutes < minimum_online_minute));
            }

            const car_desc_symbol = createTextSymbol(point, name + "\n" + "\n");
            view.graphics.add(car_symbol);
            view.graphics.add(car_desc_symbol);

            const car = new Car(name, car_symbol, car_desc_symbol, latitude, longitude);
            cars.push(car);

        } catch (e) {
            console.log(e.toString());
        }
    }

    function UpdateDeviceStatus(name, last_on) {
        try {
            var startDate = new Date(1000 * last_on);
            // Do your operations
            var endDate = new Date();
            var minutes = ((endDate.getTime() - startDate.getTime()) / 1000) / 60;

            const icon = document.getElementById(name.toString().trim()).firstChild;
            icon.style.color = (minutes < minimum_online_minute) ? online_color : offline_color;
        } catch (e) {

        }
    }

    function ViewChangeListening(name) {

        for (i = 0; i < cars.length; i++) {
            const car = cars[i];
            if (car.name.toString().trim() === name.toString().trim()) {
                console.log('true');

                try {
                    view.center = {
                        type: "point",
                        longitude: car.lng,
                        latitude: car.lat,
                    };
                    view.scale = 2500;
                    view.tilt = 50;

                } catch (e) {
                    console.log(e.toString());
                }

            }
        }
    }

    function createDataRequest(host, port) {
        setInterval(function () {
            try {
                console.log(new Date());

                LoadDeviceData();

                const track_status = track_car.viewModel.tracking;

                if (track_status) {
                    if (track_uuid !== '') {
                        console.log('Get Device');
                        GetGPS(track_uuid);
                    }
                }
            } catch (e) {

            }
        }, 60 * 1000);
    }

    function LoadDeviceData() {
        $.get('?r=device/get-devices', function (res) {

            Object.keys(res).forEach(function (index) {
                console.log(res[index].name);
                const name = res[index].name;
                const last_on = res[index].last_on;
                const latitude = res[index].lat;
                const longitude = res[index].lng;
                const monitor = res[index].monitor;
                const update_interval = res[index].update_interval;

                try {
                    var car_index = cars.findIndex(obj => obj.name === name);
                    if (car_index < 0) {
                        location.reload();
                    }

                    UpdateDeviceStatus(name, last_on);
                    UpdateDeviceLocation(name, latitude, longitude, last_on);

                    var device_index = devicesAutoRequest.findIndex(obj => obj.name === name);

                    if (monitor == 0) {
                        if (device_index >= 0) {
                            devicesAutoRequest.splice(device_index, 1);
                        }
                    } else {
                        if (device_index >= 0) {
                            var d = devicesAutoRequest.find(o => o.name === name);
                            d.monitor = monitor;
                            d.update_interval = update_interval;
                            devicesAutoRequest.splice(device_index, 1);
                            devicesAutoRequest.push(d);
                        } else {
                            devicesAutoRequest.push(new Device(name, monitor, update_interval));
                        }
                    }
                } catch (e) {
                    console.log(e.toString());
                }
            });
        }, 'json').fail(function (e) {

        });
    }

    function GetGPS(name) {
        $.get('?r=device/get-device-gps' + '&uid=' + name, function (res) {
            console.log('get device ' + name + ' success');
        }, 'json').fail(function (e) {

        });
    }

    function createConnection(host, port) {
        try {
            var socket = io.connect(host + ':' + port, {
                reconnection: true,
                reconnectionDelay: 1000,
                reconnectionDelayMax: 5000,
                reconnectionAttempts: Infinity
            });

            // Event handler for new connections.
            // The callback function is invoked when a connection with the
            // server is established.
            socket.on('connect', function () {
                console.log('connected');
                socket.emit('new message', {data: 'I\'m connected!'});
            });

            socket.on('disconnect', function () {
                console.log('disconnected');
                window.setTimeout(this, 30000);
            });

            // Event handler for server sent data.
            // The callback function is invoked whenever the server emits data
            // to the client. The data is then displayed in the "Received"
            // section of the page.
            socket.on('new message', function (msg) {
                console.log(msg.message);
                try {
                    let message = JSON.parse(msg.message.toString());
                    const lat = message.lat;
                    const lng = message.lng;
                    const name = message.device;
                    const last_on = message.last_on;

                    if (lat > 0 && lng > 0) {
                        UpdateDeviceStatus(name, last_on);
                        UpdateDeviceLocation(name, lat, lng, last_on);

                        if (track_uuid_focus != '' && track_uuid_focus === name) {
                            // change focus
                            ViewChangeListening(track_uuid_focus);
                        }
                    }

                } catch (e) {
                    console.log(e.toString());
                }
            });
        } catch (e) {
            console.log(e.toString());
        }
    }

    function createSearchTimeline() {

        const contend = document.getElementById("timeline");

        const layerListExpand = new Expand({
            expandIconClass: "esri-icon-polyline",  // see https://developers.arcgis.com/javascript/latest/guide/esri-icon-font/
            // expandTooltip: "Expand LayerList", // optional, defaults to "Expand" for English locale
            view: view,
            content: contend,
        });
        view.ui.add(layerListExpand, "top-right");

        // typical usage
        var date_start = new DatePicker({
            id: "date_start",
            container: "date-picker_start", // DOM element for widget
            value: new Date(), // value that will initially display
        });

        var date_end = new DatePicker({
            id: "date_start",
            container: "date-picker_end", // DOM element for widget
            value: new Date(), // value that will initially display
        });

        contend.classList.add("show");

        TrackInterval();

        graphicsLayer = new GraphicsLayer();
        view.map.add(graphicsLayer);

        track_timeline = new Track({
            container: 'searchTimeline',
            view: view,
            graphic: new Graphic({
                symbol: {
                    type: "picture-marker",  // autocasts as new PictureMarkerSymbol()
                    url: "/img/car_red.png",
                    width: "50px",
                    height: "50px"
                },
            }),
            goToLocationEnabled: false // disable this since we want to control what happens after our location is acquired
        });

        view.when(function () {

            var prevLocation = view.center;

            track_timeline.on("track", function () {
                var location = track_timeline.graphic.geometry;

                let timeline_point = new Graphic({
                    geometry: {
                        type: "point", // autocasts as new Point()
                        longitude: location.longitude,
                        latitude: location.latitude,
                    },
                    symbol: {
                        type: "simple-marker",
                        color: [255, 61, 0, 1],
                        size: "8px",  // pixels
                        outline: {  // autocasts as new SimpleLineSymbol()
                            color: [255, 255, 255],
                            width: 1  // points
                        }
                    }
                });

                // add graphics location
                view.graphics.add(timeline_point);
                timeline_symbols.push(timeline_point);

                if (last_latitude !== 0 && last_longitude !== 0) {
                    let polylineGraphic = new Graphic({
                        geometry: {
                            type: "polyline", // autocasts as new Polyline()
                            paths: [
                                [last_longitude, last_latitude],
                                [location.longitude, location.latitude],
                            ]
                        },
                        symbol: {
                            type: "simple-line", // autocasts as SimpleLineSymbol()
                            color: [226, 119, 40],
                            width: 2
                        },
                    });
                    graphicsLayer.add(polylineGraphic);
                    timeline_symbols.push(polylineGraphic);

                }

                view
                    .goTo({
                        center: location,
                        tilt: 50,
                        scale: 25000,
                        heading: 360 - getHeading(location, prevLocation), // only applies to SceneView
                        rotation: 360 - getHeading(location, prevLocation) // only applies to MapView
                    })
                    .catch(function (error) {
                        if (error.name != "AbortError") {
                            console.error(error);
                        }
                    });

                prevLocation = location.clone();
                last_latitude = location.latitude;
                last_longitude = location.longitude;

            });

            track_timeline.stop();
        });
    }

    function getHeading(point, oldPoint) {
        // get angle between two points
        var angleInDegrees =
            (Math.atan2(point.y - oldPoint.y, point.x - oldPoint.x) * 180) /
            Math.PI;

        // move heading north
        return -90 + angleInDegrees;
    }

    function TrackInterval() {
        let currentCoordIndex = 0;
        geolocate.use();

        setInterval(function () {

            if (coords.length > 0) {
                geolocate.change(coords[currentCoordIndex]);
                var date = new Date(coords[currentCoordIndex].created_at * 1000);
                document.getElementById("date_label").innerHTML = date.toLocaleString();
                currentCoordIndex = currentCoordIndex + 1;
            } else {
                track_timeline.stop();
                currentCoordIndex = 0;
                last_latitude = 0;
                last_longitude = 0;

            }

            Object.keys(devicesAutoRequest).forEach(function (index) {
                var d = devicesAutoRequest[index];
                d.count_interval = d.count_interval + 1;

                if (d.count_interval == d.update_interval) {
                    d.count_interval = 0;
                    GetGPS(d.name);
                }

                devicesAutoRequest[index] = d;
            });
        }, 1000);
    }

    function createTrackCar() {
        const contend = document.getElementById("trackwidget");

        const layerListExpand = new Expand({
            expandIconClass: "esri-icon-tracking",  // see https://developers.arcgis.com/javascript/latest/guide/esri-icon-font/
            // expandTooltip: "Expand LayerList", // optional, defaults to "Expand" for English locale
            view: view,
            content: contend,
        });
        view.ui.add(layerListExpand, "top-right");

        contend.classList.add("show");

        track_car = new Track({
            view: view,
            container: 'trackcarbtn',
            graphic: new Graphic({
                symbol: {
                    type: "picture-marker",  // autocasts as new PictureMarkerSymbol()
                    url: "/img/car_red.png",
                    width: "50px",
                    height: "50px"
                },
            }),
            goToLocationEnabled: false // disable this since we want to control what happens after our location is acquired
        });

        view.when(function () {
            //var prevLocation = view.center;
            var prevLocation = null;

            track_car.on("track", function () {
                var location_track = track_car.graphic.geometry;
                if (prevLocation == null) {
                    prevLocation = location_track;
                }

                view
                    .goTo({
                        center: location_track,
                        tilt: 50,
                        scale: 25000,
                        heading: (prevLocation.latitude === location_track.latitude) ? 360 : 360 - getHeading(location_track, prevLocation), // only applies to SceneView
                        rotation: (prevLocation.latitude === location_track.latitude) ? 360 : 360 - getHeading(location_track, prevLocation) // only applies to MapView
                    })
                    .catch(function (error) {

                    });

            });

            track_car.stop();
        });

    }

    function createCarSymbol(point, isOnline) {
        // Todo Test add Device
        //view.graphics.add();
        return new Graphic({
            geometry: point,
            symbol: {
                type: "picture-marker",  // autocasts as new PictureMarkerSymbol()
                url: (isOnline) ? "/img/suv-car-red.png" : "/img/suv-car-grey.png",
                width: "50px",
                height: "50px"
            },
        });
    }

    function createTrackCarSymbol(point) {
        return new Graphic({
            geometry: point,
            symbol: {
                type: "picture-marker",  // autocasts as new PictureMarkerSymbol()
                url: "/img/car_red.png",
                width: "50px",
                height: "50px"
            },

        });
    }

    function createTextSymbol(point, text) {
        return new Graphic({
            geometry: point,
            symbol: {
                type: "text",  // autocasts as new TextSymbol()
                color: "white",
                haloColor: "black",
                haloSize: "1px",
                text: text,
                xoffset: 3,
                yoffset: 30,
                verticalAlignment: "top",
                horizontalAlignment: "right",
                lineHeight: 6,
                font: {  // autocasts as new Font()
                    size: 8,
                    family: "Josefin Slab",
                    weight: "normal"
                }
            }
        });
    }

});




