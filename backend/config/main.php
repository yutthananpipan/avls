<?php
$params = array_merge(
    require __DIR__ . '/../../common/config/params.php',
    require __DIR__ . '/../../common/config/params-local.php',
    require __DIR__ . '/params.php',
    require __DIR__ . '/params-local.php'
);

return [
    'id' => 'app-backend',
    'name'=>'AVLS',
    'basePath' => dirname(__DIR__),
    'controllerNamespace' => 'backend\controllers',
    'bootstrap' => ['log'],
    'language' => 'th',
    'modules' => [
        'user' => [
            'class' => 'dektrium\user\Module',
            'enableUnconfirmedLogin' => false,
            'enablePasswordRecovery' => false,
            'enableRegistration' => false,
            'confirmWithin' => 21600,
            'cost' => 12,
            'admins' => ['admin', 'sam'],
            'modelMap' => [
                'Profile' => 'common\models\Profile',
                'User' => 'common\models\User',
                'UserSearch' => 'common\models\UserSearch',

                'RecoveryForm' => 'common\models\RecoveryForm',
                'RegistrationForm' => 'common\models\RegistrationForm',
                'ResendForm' => 'common\models\ResendForm',
                'SettingsForm' => 'common\models\SettingsForm',

            ],

            'controllerMap' => [
                'settings' => 'backend\controllers\SettingsController',
                'admin' => 'backend\controllers\AdminController',
                'security' => 'backend\controllers\SecurityController',
                'recovery' => 'backend\controllers\RecoveryController',
                'registration' => 'backend\controllers\RegistrationController'
            ],

        ],
        'admin' => [
            'class' => 'mdm\admin\Module',
            'layout' => 'left-menu',
            'controllerMap' => [
                'assignment' => [
                    'class' => 'mdm\admin\controllers\AssignmentController',
                    'userClassName' => 'common\models\User',
                    //เรียกใช้โมเดล user ของ dektrium
                ]
            ],
        ],
    ],
    'components' => [
        'request' => [
            'csrfParam' => '_csrf-backend',
        ],
        'view' => [
            'theme' => [
                'pathMap' => [
                    '@dektrium/user/views' => '@backend/views/user'
                ],
            ],
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'user' => [
            'identityClass' => 'common\models\User',
            'enableSession' => true,
            'enableAutoLogin' => true,
            'authTimeout'=>3600 * 8,  //Number of second to Automatic Logout if inactive
        ],'session' => [
            'class' => 'yii\web\Session',
            'cookieParams' => ['httponly' => true, 'lifetime' => 3600 * 8],
            'timeout' => 3600 * 8, //session expire
            'useCookies' => true,
        ],
    ],
    'as access' => [
        'class' => 'mdm\admin\components\AccessControl',
        'allowActions' => [

            'user/security/*',
            'user/registration/*',
            'user/settings/*',
            'user/recovery/*',

            // Todo Permission::
            //'gii/*',
            'admin/*',
        ]
    ],
    'params' => $params,
];
