<?php

/* @var $this yii\web\View */

use kartik\date\DatePicker;
use kartik\form\ActiveForm;
use yii\bootstrap\Modal;
use yii\helpers\Html;
use yii\helpers\Url;

$this->title = 'My Yii Application';

?>

<div class="contain-side">
    <div id="panel-menu" class="panel-side">
        <h4 id="head" class="mt-5">Device Tracking</h4>
        <ul id="nyc_graphics">
            <li>Loading&hellip;</li>
        </ul>
    </div>

    <div id="map"></div>

    <div id="timeline" style="display: none;">
        <h5 style="padding-left: 5px;color: white; font-weight: bold;">ไทม์ไลน์</h5>

        <div class="dropdown">
            <span style="color: white; padding-right: 10px;">เลือกอุปกรณ์</span>
            <button id="timeline_btn" onclick="timelineShow()" class="dropbtn">รหัส

            </button>
        </div>

        <div>
            <span id="date-picker_start" style="width: 120px;"></span>
            <span style="color: white; padding: 8px;">ถึง</span>
            <span id="date-picker_end" style="width: 120px;"></span>
        </div>

        <div style="padding-right: 7px;">
            <span style="font-size: 14px; color: #f0f0f0;" id="date_label"></span>
            <div id="searchTimeline" style="float: right;" data-dismiss="modal"></div>
        </div>
    </div>

    <div id="trackwidget" style="display: none;">
        <h5 style="color: white; font-weight: bold; padding-left: 5px;">ติดตามตำแหน่งแบบเรียบไทม์</h5>

        <div class="dropdown">
            <span style="color: white; padding-right: 10px;">เลือกอุปกรณ์</span>
            <button id="track_dropdown_btn" onclick="trackShow()" class="dropbtn">รหัส
            </button>
        </div>

        <div class="float-right" style="margin-right: 3px;">
            <div id="trackcarbtn" style="float: right;" data-dismiss="modal"></div>
        </div>
    </div>

    <div id="TrackDropdown" class="dropdown-content list-track-dropdown" style="padding: 0px;">
        <div id="TrackList">
            <li>Loading&hellip;</li>
        </div>
    </div>

    <div id="TimelineDropdown" class="dropdown-content list-timeline-dropdown" style="padding: 0px;">
        <div id="DeviceList">
            <li>Loading&hellip;</li>
        </div>
    </div>

</div>

<script>
    /* When the user clicks on the button,
    toggle between hiding and showing the dropdown content */
    function timelineShow() {
        document.getElementById("TimelineDropdown").classList.toggle("show");
    }

    function trackShow() {
        document.getElementById("TrackDropdown").classList.toggle("show");
    }

    // Close the dropdown if the user clicks outside of it
    window.onclick = function (event) {
        if (!event.target.matches('.dropbtn')) {
            var dropdowns = document.getElementsByClassName("dropdown-content");
            var i;
            for (i = 0; i < dropdowns.length; i++) {
                var openDropdown = dropdowns[i];
                if (openDropdown.classList.contains('show')) {
                    openDropdown.classList.remove('show');
                }
            }
        }
    };
</script>



