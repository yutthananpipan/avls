<?php

use common\models\Area;
use kartik\date\DatePicker;
use kartik\widgets\Select2;
use yii\bootstrap\Modal;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Device */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="device-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true, 'disabled' => ($model->name == null) ? false : true]) ?>

    <?= $form->field($model, 'car_description')->textInput() ?>

    <?= $form->field($model, 'update_interval')->textInput() ?>

    <?= $form->field($model, 'monitor')->dropDownList(['1' => 'ใช้งาน', '0' => 'ไม่ใช้งาน',]) ?>

    <?= $form->field($model, 'comport')
        ->dropDownList(Area::getAreas()) ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('user', 'Save'), ['class' => 'btn btn-dark']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
