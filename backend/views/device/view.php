<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Device */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'อุปกรณ์', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div style="padding-top: 60px;" class="panel m-3">

    <div class="panel-heading">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="?r=/device/index"><?= Yii::t('user', 'หน้าหลัก') ?></a></li>
                <li class="breadcrumb-item"><a href="?r=/device/index"><?= Yii::t('user', 'จัดการอุปกรณ์') ?></a></li>
                <li class="breadcrumb-item active" aria-current="page">ข้อมูลอุปกรณ์</li>
            </ol>
        </nav>
        <h1><?= Html::encode($this->title) ?></h1>
    </div>

    <div class="panel-body">
        <p>
            <?= Html::a(Yii::t('user', 'Update'), ['update', 'id' => $model->name], ['class' => 'btn btn-dark']) ?>
            <?= Html::a(Yii::t('user', 'Delete'), ['delete', 'id' => $model->name], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => 'Are you sure you want to delete this item?',
                    'method' => 'post',
                ],
            ]) ?>
        </p>

        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
                'name',
                'car_description',
                'Lat',
                'Lng',
                [
                    'attribute' => 'last_on',
                    'value' => function ($model) {
                        return Yii::t('user', '{0, date, MMMM dd, YYYY HH:mm}', [$model->last_on]);
                    }
                ],
                'update_interval',
                [
                    'label' => 'comport',
                    'value' => $model->comport,
                ],
                [
                    'attribute' => 'monitor',
                    'value' => ($model->monitor == 0) ? 'ไม่ใช้งาน' : 'ใช้งาน',
                ]
            ],
        ]) ?>
    </div>

</div>
