<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Device */

$this->title = 'แก้ไขอุปกรณ์หมายเลข: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Devices', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'name' => $model->name]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div style="padding-top: 60px;" class="panel">

    <div class="panel-heading">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="?r=/device/index"><?= Yii::t('user', 'หน้าหลัก') ?></a></li>
                <li class="breadcrumb-item"><a href="?r=/device/index"><?= Yii::t('user', 'จัดการอุปกรณ์') ?></a></li>
                <li class="breadcrumb-item active" aria-current="page">แก้ไขข้อมูล</li>
            </ol>
        </nav>
        <h1><?= Html::encode($this->title) ?></h1>
    </div>

    <div class="panel-body">
        <?= $this->render('_form', [
            'model' => $model,
        ]) ?>
    </div>

</div>
