<?php

use common\models\Area;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\DeviceSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'ระบบจัดการอุปกรณ์';
$this->params['breadcrumbs'][] = $this->title;
?>
<div style="padding-top: 70px;" class="panel">

    <div class="panel-heading">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="?r=device"><?= Yii::t('user', 'หน้าหลัก') ?></a></li>
            </ol>
        </nav>

        <h1><?= Html::encode($this->title) ?></h1>
        <?= $this->render('@app/views/User/_alert', ['module' => Yii::$app->getModule('user')]) ?>
    </div>


    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <div class="panel-body">

        <p>
            <?= Html::a(Yii::t('user', 'Create'), ['create'], ['class' => 'btn btn-dark']) ?>
        </p>

        <?php Pjax::begin(); ?>
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'options' => ['class' => 'table-hover table-responsive'],
            'headerRowOptions' => ['class' => 'text-center'],
            'tableOptions' => [
                'class' => 'table table-striped table-hover table-bordered',
            ],
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],

                'name',
                [
                    'attribute' => 'Lat',
                    'value' => function ($model) {
                        return ($model->Lat == null) ? '' : Yii::t('user', $model->Lat);
                    }
                ],
                [
                    'attribute' => 'Lng',
                    'value' => function ($model) {
                        return ($model->Lng == null) ? '' : Yii::t('user', $model->Lng);
                    }
                ],
                [
                    'attribute' => 'car_description',
                    'value' => function ($model) {
                        return ($model->car_description == null) ? '' : Yii::t('user', $model->car_description);
                    }
                ],
                'update_interval',
                [
                    'attribute' => 'last_on',
                    'value' => function ($model) {
                        return Yii::t('user', '{0, date, MMMM dd, YYYY HH:mm}', [$model->last_on]);
                    }
                ],
                [
                    'attribute' => 'comport',
                    'value' => function ($model) {
                        return $model->comport;
                    },
                ],
                [
                    'header' => 'area',
                    'headerOptions' => ['style' => 'color: #111;'],
                    'value' => function ($model) {
                        $province = '';
                        try {
                            $province = Yii::t('user', ($model->comport == 0) ? '' : Area::getAreaName($model->comport));
                        } catch (Exception $exception) {
                        }
                        return $province;
                    },
                ],
                [
                    'attribute' => 'monitor',
                    'value' => function ($model) {
                        if ($model->monitor == 0) {
                            return Html::a('เปิดการใช้งาน', ['monitor', 'id' => $model->name], [
                                'class' => 'btn btn-xs btn-success btn-block',
                                'data-method' => 'post',
                                'data-confirm' => 'คุณแน่ใจว่าต้องการเปิดการใช้งานอุปกรณ์นี้?',
                            ]);
                        } else {
                            return Html::a('ปิดการใช้งาน', ['monitor', 'id' => $model->name], [
                                'class' => 'btn btn-xs btn-danger btn-block',
                                'data-method' => 'post',
                                'data-confirm' => 'คุณแน่ใจว่าต้องการปิดการใช้งานอุปกรณ์นี้?',
                            ]);
                        }
                    },
                    'format' => 'raw',
                ],

                [
                    'class' => 'yii\grid\ActionColumn',
                    'template' => '<div class="btn-group btn-group-sm text-center" role="group">{update} {delete} </div>',
                    'buttonOptions' => ['class' => 'btn btn-default'],
                    'options' => ['style' => 'width:120px;'],
                    'buttons' => [
                        'view' => function ($url, $model, $key) {
                            return Html::a('<i class="glyphicon glyphicon-eye-open"></i>', $url, ['class' => 'btn btn-default']);
                        },
                        'update' => function ($url, $model, $key) {
                            return Html::a('<i class="glyphicon glyphicon-pencil"></i>', $url, ['class' => 'btn btn-default']);
                        },
                        'delete' => function ($url, $model, $key) {
                            return Html::a('<i class="glyphicon glyphicon-trash"></i>', $url, [
                                'class' => 'btn btn-default text-danger',
                                'data-confirm' => Yii::t('user', 'คุณต้องการลบข้อมูลนี้หรือไม่'),
                                'data-method' => 'POST',
                            ]);
                        },
                    ],
                ],
            ],
        ]); ?>
        <?php Pjax::end(); ?>
    </div>

    <?php Pjax::end(); ?>

</div>
