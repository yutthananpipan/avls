<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Device */

$this->title = 'สร้างข้อมูล';
$this->params['breadcrumbs'][] = ['label' => 'Devices', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div style="padding-top: 60px;" class="panel">

    <div class="panel-heading">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="?r=/site/index"><?= Yii::t('user', 'หน้าหลัก') ?></a></li>
                <li class="breadcrumb-item"><a href="?r=/device/index"><?= Yii::t('user', 'จัดการอุปกรณ์') ?></a></li>
                <li class="breadcrumb-item active" aria-current="page">สร้างข้อมูล</li>
            </ol>
        </nav>
        <h1><?= Html::encode($this->title) ?></h1>
    </div>

    <div class="panel-body">
        <?= $this->render('_form', [
            'model' => $model,
        ]) ?>
    </div>

</div>
