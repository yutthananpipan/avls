<?php

/*
 * This file is part of the Dektrium project.
 *
 * (c) Dektrium project <http://github.com/dektrium>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 */

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/**
 * @var yii\web\View $this
 * @var yii\widgets\ActiveForm $form
 * @var dektrium\user\models\SettingsForm $model
 */

$this->title = Yii::t('user', 'ตั้งค่าบัญชี');
$this->params['breadcrumbs'][] = $this->title;
?>

<div style="padding-top: 60px;" class="panel">
    <div class="panel-heading">
        <?= $this->render('/_alert', ['module' => Yii::$app->getModule('user')]) ?>
    </div>
    <div class="panel-body">
        <div class="row">
            <div class="col-md-3 ">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <?= $this->render('_menu') ?>
                    </div>
                </div>
            </div>

            <div class="col-md-9 pb-5">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <h4 class="pt-10 pl-3" style="font-weight: bold;"><?= Html::encode($this->title) ?></h4>
                        <br>
                        <div>
                            <?php $form = ActiveForm::begin([
                                'id' => 'account-form',
                                'options' => ['class' => 'form-horizontal'],
                                'fieldConfig' => [
                                    'template' => "{label}\n<div class=\"col-lg-9\">{input}</div>\n<div class=\"col-sm-offset-3 col-lg-9\">{error}\n{hint}</div>",
                                    'labelOptions' => ['class' => 'col-lg-3 control-label'],
                                ],
                                'enableAjaxValidation' => true,
                                'enableClientValidation' => false,
                            ]); ?>

                            <?= $form->field($model, 'email') ?>

                            <?= $form->field($model, 'username')->textInput(['disabled' => true]) ?>

                            <?= $form->field($model, 'new_password')->passwordInput() ?>

                            <hr/>

                            <?= $form->field($model, 'current_password')->passwordInput() ?>

                            <div style="float: right"><?= Html::submitButton(Yii::t('user', 'Save'), ['class' => 'btn btn-dark text-uppercase text-right']) ?></div>

                            <?php ActiveForm::end(); ?>
                        </div>
                    </div>
                </div>
                <?php if ($model->module->enableAccountDelete): ?>
                    <div style="font-size: 15px;" class="pl-sm-5 pr-sm-5">
                        <h4 class="pt-10 pl-3" style="font-weight: bold;"><?= Yii::t('user', 'Delete account') ?></h4>
                        <br>
                        <div class="filters-content">
                            <p>
                                <?= Yii::t('user', 'Once you delete your account, there is no going back') ?>.
                                <?= Yii::t('user', 'It will be deleted forever') ?>.
                                <?= Yii::t('user', 'Please be certain') ?>.
                            </p>
                            <?= Html::a(Yii::t('user', 'Delete account'), ['delete'], [
                                'class' => 'btn btn-danger',
                                'data-method' => 'post',
                                'data-confirm' => Yii::t('user', 'Are you sure? There is no going back'),
                            ]) ?>
                        </div>
                    </div>
                <?php endif ?>
            </div>
        </div>
    </div>
</div>

<script>
    path = '?r=user/settings/profile';
    subpath = path;

    var target = $('nav a[href="' + path + '"]');
    // Add active class to target link
    target.addClass('menu-active');


</script>
