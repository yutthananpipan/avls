<?php

/*
 * This file is part of the Dektrium project.
 *
 * (c) Dektrium project <http://github.com/dektrium>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 */

use yii\helpers\Html;
use dektrium\user\helpers\Timezone;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

/**
 * @var yii\web\View $this
 * @var yii\widgets\ActiveForm $form
 * @var dektrium\user\models\Profile $model
 */

$this->title = Yii::t('user', 'ตั้งค่าข้อมูลส่วนตัว');
$this->params['breadcrumbs'][] = $this->title;
?>
<div style="padding-top: 60px;" class="panel">
    <div class="panel-heading">
        <?= $this->render('/_alert', ['module' => Yii::$app->getModule('user')]) ?>
    </div>
    <div class="panel-body">
        <div class="row">
            <div class="col-md-3 ">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <?= $this->render('_menu') ?>
                    </div>
                </div>
            </div>

            <div class="col-md-9">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <h4 class="pt-10" style="font-weight: bold;"><?= Html::encode($this->title) ?></h4>
                        <br>
                        <div>
                            <?php $form = ActiveForm::begin([
                                'id' => 'profile-form',
                                'options' => ['class' => 'form-horizontal', 'enctype' => 'multipart/form-data'],
                                'enableAjaxValidation' => true,
                                'enableClientValidation' => false,
                                'validateOnBlur' => false,
                            ]); ?>

                            <div class="pl-4 pr-4">
                                <?= $form->field($model, 'name')->textInput(['maxlength' => true, 'disabled' => false]) ?>

                                <?= $form->field($model, 'public_email')->textInput(['maxlength' => true]) ?>
                            </div>

                            <div style="float: right"><?= Html::submitButton(Yii::t('user', 'Save'), ['class' => 'btn btn-dark text-uppercase text-right']) ?></div>

                            <?php ActiveForm::end(); ?>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<script>
    path = '?r=user/settings/profile';
    subpath = path;

    var target = $('nav a[href="' + path + '"]');
    // Add active class to target link
    target.addClass('menu-active');


</script>
