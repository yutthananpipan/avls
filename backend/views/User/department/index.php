<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel common\models\DepartmentSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Departments';
$this->params['breadcrumbs'][] = $this->title;
?>
<?= $this->render('../admin/_mainmenu') ?>

<?= $this->render('../_alert', ['module' => Yii::$app->getModule('user')]) ?>

<div class="department-index">

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'headerRowOptions' => ['class' => 'text-center'],
        'options' => ['class' => 'table-hover table-responsive'],
        'layout' => "{items}\n{pager}",
        'pager' => [
            'options' => [
                'class' => 'pagination',
                'style' => ['margin-left' => '15px'],
            ],

            // Customzing CSS class for pager link
            'linkOptions' => ['class' => 'page-link'],
            'activePageCssClass' => 'active',
            'disabledPageCssClass' => 'disable',

            // Customzing CSS class for navigating link
            'prevPageCssClass' => 'mypre',
            'nextPageCssClass' => 'mynext',
            'firstPageCssClass' => 'myfirst',
            'lastPageCssClass' => 'mylast',

            //'firstPageLabel' => 'first',
            //'lastPageLabel' => 'last',
            //'prevPageLabel' => '<span class="glyphicon glyphicon-chevron-left"></span>',
            //'nextPageLabel' => '<span class="glyphicon glyphicon-chevron-right"></span>',
        ],
        'columns' => [
            [
                'attribute' => 'id',
                'headerOptions' => ['style' => 'width:80px; color:#777777;'], # 90px is sufficient for 5-digit user ids
                'value' => 'id'
            ],
            'name',
            [
                'class' => 'yii\grid\ActionColumn',
                'header' => 'จัดการ',
                'buttonOptions' => ['class' => 'btn btn-default'],
                'template' => '<div class="btn-group btn-group-sm" role="group" aria-label="...">{update}{delete}</div>',
                'options' => ['style' => 'width:80px;'],
                'buttons' => [
                    'view' => function ($url, $model, $key) {
                        return Html::a('<i class="	fas fa-eye"></i>', $url,
                            [
                                'class' => 'btn btn-sm btn-outline-secondary text-info',
                                'title' => Yii::t('user', 'แก้ไขข้อมูลผู้ใช้งาน'),
                            ]);
                    },
                    'update' => function ($url, $model, $key) {
                        return Html::a('<i class="	fas fa-edit"></i>', $url,
                            [
                                'class' => 'btn btn-sm btn-outline-secondary text-info',
                                'title' => Yii::t('user', 'แก้ไขข้อมูลผู้ใช้งาน'),
                            ]);
                    },
                    'delete' => function ($url, $model, $key) {
                        return Html::a('<i class="fa fa-trash"></i>', $url, [
                            'class' => 'btn btn-sm btn-outline-secondary text-danger',
                            'title' => Yii::t('user', 'ลบข้อมูลผู้ใช้งาน'),
                            'data-confirm' => Yii::t('user', 'คุณต้องการลบข้อมูลนี้หรือไม่'),
                            'data-method' => 'POST',
                        ]);
                    },
                ]
            ],
        ],
    ]); ?>

    <div class="row home-about-right pl-90">
        <div class="col-12">
            <div class="form-group">
                <?= Html::a('เพิ่มข้อมูล', ['create'], ['class' => 'primary-btn text-uppercase float-right']) ?>
            </div>
        </div>
    </div>
</div>
