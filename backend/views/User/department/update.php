<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Department */

$this->title = 'แก้ไข: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Departments', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>

<?= $this->render('../admin/_mainmenu') ?>

<div class="department-update">
    <?= $this->render('../_alert', ['module' => Yii::$app->getModule('user')]) ?>
    <h5 style="font-weight: bold;"><?= $this->title ?></h5>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
