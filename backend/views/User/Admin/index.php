<?php

/*
 * This file is part of the Dektrium project.
 *
 * (c) Dektrium project <http://github.com/dektrium>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 */

use yii\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Pjax;


/**
 * @var \yii\web\View $this
 * @var \yii\data\ActiveDataProvider $dataProvider
 * @var common\models\UserSearch $searchModel
 */

$this->title = Yii::t('user', 'จัดการสมาชิก');
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="p-1 panel">
    <div class="panel-heading">
        <?= $this->render('_menu') ?>
    </div>

    <div class="panel-body">
        <?= $this->render('/_alert', ['module' => Yii::$app->getModule('user')]) ?>

        <?php Pjax::begin() ?>

        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'options' => ['class' => 'table-hover table-responsive'],
            'headerRowOptions' => ['class' => 'text-center'],
            'layout' => "{items}\n{pager}",
            'pager' => [
                'options' => [
                    'class' => 'pagination',
                    //'style' => ['margin-left' => '15px'],
                ],

                // Customzing CSS class for pager link
                'linkOptions' => ['class' => 'page-link'],
                'activePageCssClass' => 'active',
                'disabledPageCssClass' => 'disable',

                // Customzing CSS class for navigating link
                'prevPageCssClass' => 'mypre',
                'nextPageCssClass' => 'mynext',
                'firstPageCssClass' => 'myfirst',
                'lastPageCssClass' => 'mylast',

                //'firstPageLabel' => 'first',
                //'lastPageLabel' => 'last',
                //'prevPageLabel' => '<span class="glyphicon glyphicon-chevron-left"></span>',
                //'nextPageLabel' => '<span class="glyphicon glyphicon-chevron-right"></span>',
            ],
            'columns' => [
                [
                    'attribute' => 'id',
                    'value' => 'id',
                    'headerOptions' => ['style' => 'width:30px;'],
                ],
                [
                    'attribute' => 'roles',
                    'headerOptions' => ['style' => 'width:140px;'],
                    'value' => function ($data) {
                        $roles = \Yii::$app->authManager->getRolesByUser($data->id);
                        if ($roles) {
                            return implode(', ', array_keys($roles));
                        } else {
                            return '';
                        }
                    }

                ],
                [
                    'headerOptions' => ['style' => 'width:120px;'],
                    'attribute' => 'username',
                    'value' => 'username',

                ],
                [
                    'headerOptions' => ['style' => 'width:140px;'],
                    'attribute' => 'profile.name',
                    'value' => function ($model) {
                        if ($model->profile->name != null) {
                            return $model->profile->name;
                        } else {
                            return '';
                        }
                    },
                ],

                [
                    'attribute' => 'email',
                    'value' => 'email',

                ],
                [
                    'attribute' => 'last_login_at',
                    'value' => function ($model) {
                        if (!$model->last_login_at || $model->last_login_at == 0) {
                            return Yii::t('user', 'Never');
                        } else if (extension_loaded('intl')) {
                            return Yii::t('user', '{0, date, MMMM dd, YYYY HH:mm}', [$model->last_login_at]);
                        } else {
                            return date('Y-m-d G:i:s', $model->last_login_at);
                        }
                    },
                ],
                [
                    'header' => Yii::t('user', 'Confirmation'),
                    'value' => function ($model) {
                        if ($model->isConfirmed) {
                            return '<div class="text-center">
                                <span class="text-success">' . Yii::t('user', 'Confirmed') . '</span>
                            </div>';
                        } else {
                            return Html::a(Yii::t('user', 'Confirm'), ['confirm', 'id' => $model->id], [
                                'class' => 'btn btn-xs btn-success btn-block',
                                'data-method' => 'post',
                                'data-confirm' => Yii::t('user', 'Are you sure you want to confirm this user?'),
                            ]);
                        }
                    },
                    'format' => 'raw',
                    'visible' => Yii::$app->getModule('user')->enableConfirmation,
                ],
                [
                    'header' => Yii::t('user', 'Block status'),
                    'value' => function ($model) {
                        if ($model->isBlocked) {
                            return Html::a(Yii::t('user', 'Unblock'), ['block', 'id' => $model->id], [
                                'class' => 'btn btn-xs btn-success btn-block',
                                'data-method' => 'post',
                                'data-confirm' => Yii::t('user', 'Are you sure you want to unblock this user?'),
                            ]);
                        } else {
                            return Html::a(Yii::t('user', 'Block'), ['block', 'id' => $model->id], [
                                'class' => 'btn btn-xs btn-danger btn-block',
                                'data-method' => 'post',
                                'data-confirm' => Yii::t('user', 'Are you sure you want to block this user?'),
                            ]);
                        }
                    },
                    'format' => 'raw',
                ],
                [
                    'class' => 'yii\grid\ActionColumn',
                    'template'=>'<div class="btn-group btn-group-sm text-center" role="group">{update} {delete} </div>',
                    'buttonOptions'=>['class'=>'btn btn-default'],
                    'options' => ['style' => 'width:100px;'],
                    'buttons' => [
                        'update' => function ($url, $model, $key) {
                            return Html::a('<i class="glyphicon glyphicon-pencil"></i>',$url,['class'=>'btn btn-default']);
                        },
                        'delete' => function ($url, $model, $key) {
                            return Html::a('<i class="glyphicon glyphicon-trash"></i>', $url, [
                                'class' => 'btn btn-default text-danger',
                                'data-confirm' => Yii::t('user', 'คุณต้องการลบข้อมูลนี้หรือไม่'),
                                'data-method' => 'POST',
                            ]);
                        },
                    ],
                ],
            ],
        ]); ?>

        <?php Pjax::end() ?>
    </div>

</div>

<script>
    path = '?r=user/admin';
    subpath = path;

    var target = $('nav a[href="' + path + '"]');
    // Add active class to target link
    target.addClass('menu-active');

    var sub_taget = $('div a[href="' + subpath + '"]');
    sub_taget.addClass('menu-active');

</script>