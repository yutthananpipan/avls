<?php

/*
 * This file is part of the Dektrium project.
 *
 * (c) Dektrium project <http://github.com/dektrium>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 */

use yii\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;
use yii\widgets\Pjax;


/**
 * @var \yii\web\View $this
 * @var \yii\data\ActiveDataProvider $dataProvider
 * @var \dektrium\user\models\UserSearch $searchModel
 */

$this->title = Yii::t('user', 'ประวัติการใช้งาน');
$this->params['breadcrumbs'][] = $this->title;
?>

<?= $this->render('_mainmenu') ?>

<?= $this->render('/_alert', ['module' => Yii::$app->getModule('user')]) ?>

<div>
    <?php Pjax::begin() ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'options' => ['class' => 'table-hover table-responsive'],
        'headerRowOptions' => ['class' => 'text-center'],
        'layout' => "{items}\n{pager}",
        'pager' => [
            'options' => [
                'class' => 'pagination',
                //'style' => ['margin-left' => '15px'],
            ],

            // Customzing CSS class for pager link
            'linkOptions' => ['class' => 'page-link'],
            'activePageCssClass' => 'active',
            'disabledPageCssClass' => 'disable',

            // Customzing CSS class for navigating link
            'prevPageCssClass' => 'mypre',
            'nextPageCssClass' => 'mynext',
            'firstPageCssClass' => 'myfirst',
            'lastPageCssClass' => 'mylast',

            //'firstPageLabel' => 'first',
            //'lastPageLabel' => 'last',
            //'prevPageLabel' => '<span class="glyphicon glyphicon-chevron-left"></span>',
            //'nextPageLabel' => '<span class="glyphicon glyphicon-chevron-right"></span>',
        ],
        'columns' => [
            [
                'header' => 'ลำดับ',
                'class' => 'yii\grid\SerialColumn',
                'headerOptions' => ['style' => 'width:40px; color:#777777;'],
            ],
            [
                'attribute' => 'roles',
                'headerOptions' => ['style' => 'width:140px; color:#777777;'],
                'value' => function ($data) {
                    $roles = \Yii::$app->authManager->getRolesByUser($data->id);
                    if ($roles) {
                        return implode(', ', array_keys($roles));
                    } else {
                        return 'no roles';
                    }
                }

            ],
            [
                'headerOptions' => ['style' => 'color:#777777;'],
                'attribute' => 'username',
                'value' => 'username',

            ],
            [
                'headerOptions' => ['style' => 'color:#777777;'],
                'attribute' => 'last_login_at',
                'value' => function ($model) {
                    if (!$model->last_login_at || $model->last_login_at == 0) {
                        return Yii::t('user', 'Never');
                    } else if (extension_loaded('intl')) {
                        return Yii::t('user', '{0, date, MMMM dd, YYYY HH:mm}', [$model->last_login_at]);
                    } else {
                        return date('Y-m-d G:i:s', $model->last_login_at);
                    }
                },
            ],
            [
                'headerOptions' => ['style' => 'color:#777777;'],
                'attribute' => 'profile.department',
                'value' => function ($model) {
                    return  $model->profile->getDepartmentname($model->profile->department_id);
                },

            ],

        ],
    ]); ?>

    <?php Pjax::end() ?>
</div>
