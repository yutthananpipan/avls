<?php

/*
 * This file is part of the Dektrium project.
 *
 * (c) Dektrium project <http://github.com/dektrium>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 */

use yii\bootstrap\Nav;

/**
 * @var \yii\web\View $this
 * @var \dektrium\user\models\User $user
 * @var string $content
 */

$this->title = Yii::t('user', 'Update user account');
$this->params['breadcrumbs'][] = ['label' => Yii::t('user', 'Users'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="panel">
    <div class="panel-heading">
        <?= $this->render('_menu') ?>
    </div>

    <div class="panel-body">
        <div class="row">
            <div class="col-md-3">
                <div class="">
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <?= Nav::widget([
                                'options' => [
                                    'class' => 'nav flex-column nav-pills nav-stacked',
                                ],
                                'items' => [
                                    [
                                        'label' => Yii::t('user', 'บัญชีผู้ใช้งาน'),
                                        'url' => ['/user/admin/update', 'id' => $user->id]
                                    ],
                                    [
                                        'label' => Yii::t('user', 'โปรไฟล์'),
                                        'url' => ['/user/admin/update-profile', 'id' => $user->id]
                                    ],
                                    ['label' => Yii::t('user', 'สถานะ'), 'url' => ['/user/admin/info', 'id' => $user->id]],
                                    [
                                        'label' => Yii::t('user', 'จัดการบัญชี'),
                                        'url' => ['/user/admin/assignments', 'id' => $user->id],
                                        'visible' => isset(Yii::$app->extensions['dektrium/yii2-rbac']),
                                    ],
                                    '<hr>',
                                    [
                                        'label' => Yii::t('user', 'ยืนยัน'),
                                        'url' => ['/user/admin/confirm', 'id' => $user->id],
                                        'visible' => !$user->isConfirmed,
                                        'linkOptions' => [
                                            'class' => 'text-success',
                                            'data-method' => 'post',
                                            'data-confirm' => Yii::t('user', 'ยืนยันผู้ใช้งานนี้'),
                                        ],
                                    ],
                                    [
                                        'label' => Yii::t('user', 'ปิดกั้น'),
                                        'url' => ['/user/admin/block', 'id' => $user->id],
                                        'visible' => !$user->isBlocked,
                                        'linkOptions' => [
                                            'class' => 'text-danger',
                                            'data-method' => 'post',
                                            'data-confirm' => Yii::t('user', 'ปิดกั้นผู้ใช้งาน'),
                                        ],
                                    ],
                                    [
                                        'label' => Yii::t('user', 'ยกเลิกการปิดกั้น'),
                                        'url' => ['/user/admin/block', 'id' => $user->id],
                                        'visible' => $user->isBlocked,
                                        'linkOptions' => [
                                            'class' => 'text-success',
                                            'data-method' => 'post',
                                            'data-confirm' => Yii::t('user', 'ยกเลิกการปิดกั้นผู้ใช้งาน'),
                                        ],
                                    ],
                                    [
                                        'label' => Yii::t('user', 'ลบบัญชี'),
                                        'url' => ['/user/admin/delete', 'id' => $user->id],
                                        'linkOptions' => [
                                            'class' => 'text-danger',
                                            'data-method' => 'post',
                                            'data-confirm' => Yii::t('user', 'ต้องการลบผู้ใช้งานนี้หรือไม่'),
                                        ],
                                    ],
                                ],
                            ]) ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-9">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <?= $this->render('/_alert', ['module' => Yii::$app->getModule('user')]) ?>
                        <div>
                            <?= $content ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    path = '?r=user/admin';
    subpath = path;

    var target = $('nav a[href="' + path + '"]');
    // Add active class to target link
    target.addClass('menu-active');

    var sub_taget = $('div a[href="' + subpath + '"]');
    sub_taget.addClass('menu-active');

</script>
