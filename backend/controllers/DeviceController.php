<?php

namespace backend\controllers;

use common\models\Area;
use Yii;
use yii\helpers\Url;
use yii\db\Exception;
use common\models\User;
use yii\web\Controller;
use common\models\Device;
use common\models\Tracking;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use backend\models\DeviceSearch;
use yii\web\NotFoundHttpException;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use phpDocumentor\Reflection\Types\Object_;
use function GuzzleHttp\Psr7\str;

/**
 * DeviceController implements the CRUD actions for Device model.
 */
class DeviceController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],

        ];
    }

    /**
     * Lists all Device models.
     * @return mixed
     */
    public function actionIndex()
    {
        Url::remember('', 'actions-redirect');
        $searchModel = new DeviceSearch();

        $ComPorts = array();
        $auth = Yii::$app->authManager;
        $rolesUser = $auth->getRolesByUser(Yii::$app->user->getId());
        if (sizeof($rolesUser) > 0) {

            foreach ($rolesUser as $ru) {
                try {
                    array_push($ComPorts, Area::getAreaComport($ru->name));
                } catch (\Exception $exception) {
                }
            }
        }

        $dataProvider = $searchModel->searchByComport(Yii::$app->request->queryParams, $ComPorts);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Device model.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Device model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Device();

        $this->UpdateDevice($model);
        return $this->render('create', [
            'model' => $model,
        ]);
    }

    private function UpdateDevice($model)
    {
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $this->actionSetDataInterval($model->name, $model->update_interval);

            if ($model->monitor == 1) {
                $this->actionGetDeviceGps($model->name);
            }
            return $this->redirect(['view', 'id' => $model->name]);
        }
        return null;
    }

    /**
     * Updates an existing Device model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        $this->UpdateDevice($model);

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Device model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Device model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $name
     * @return Device the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($name)
    {
        if (($model = Device::findOne($name)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    public function actionMonitor($id)
    {
        $model = $this->findModel($id);
        $model->monitor = ($model->monitor == 1) ? 0 : 1;
        if ($model->save()) {
            \Yii::$app->getSession()->setFlash('success', (($model->monitor == 1) ? 'เปิดการใช้งานอุปกรณ์ ' : 'ปิดการใช้งานอุปกรณ์ ') . $model->name . ' แล้ว');

        }

        return $this->redirect(Url::previous('actions-redirect'));
    }

    public function actionGetDevices()
    {
        $auth = Yii::$app->authManager;
        $rolesUser = $auth->getRolesByUser(Yii::$app->user->getId());
        if (sizeof($rolesUser) > 0) {
            $comports = array();
            foreach ($rolesUser as $ru) {
                try {
                    array_push($comports, Area::getAreaComport($ru->name));
                } catch (\Exception $exception) {
                }
            }

            $models = Device::find()->where(['comport' => $comports])->orderBy(['last_on' => SORT_DESC])->limit(400)->all();

            $index = 0;
            $json_arrays = array();
            foreach ($models as $m) {
                $obj = new Object_();
                $obj->name = $m->name;
                $obj->lat = $m->Lat;
                $obj->lng = $m->Lng;
                $obj->last_on = $m->last_on;
                $obj->monitor = $m->monitor;
                $obj->update_interval = $m->update_interval;
                $json_arrays[$index] = $obj;
                $index++;
            }

            return json_encode($json_arrays);
        } else {
            return null;
        }
    }

    public function actionGetTimeline($name, $date_start, $date_end)
    {
        $json_arrays = array();

        try {
            $list = Tracking::find()
                ->where(['device_name' => $name,])
                ->andWhere('created_at >= :date_start', [':date_start' => $date_start])
                ->andWhere('created_at <= :date_end', [':date_end' => $date_end])
                ->orderBy(['created_at' => 'ASC'])
                ->all();

            $index = 0;

            foreach ($list as $m) {
                $obj = new Object_();
                $obj->name = $m->device_name;
                $obj->lat = $m->Lat;
                $obj->lng = $m->Lng;
                $obj->created_at = $m->created_at;

                $json_arrays[$index] = $obj;
                $index++;
            }
        } catch (\Exception $e) {
        }
        return json_encode($json_arrays);
    }

    public function actionGetDeviceStatus($uid)
    {
        $data = null;
        try {
            //Initialize cURL.
            $ch = curl_init();

            //Set the URL that you want to GET by using the CURLOPT_URL option.
            curl_setopt($ch, CURLOPT_URL, Device::HOST . '/getstatus/' . $uid);

            //Set CURLOPT_RETURNTRANSFER so that the content is returned as a variable.
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

            //Set CURLOPT_FOLLOWLOCATION to true to follow redirects.
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);

            //Execute the request.
            $data = curl_exec($ch);

            //Close the cURL handle.
            curl_close($ch);
        } catch (\Exception $e) {

        }

        return $data;
    }

    public function actionGetDeviceGps($uid)
    {
        $data = null;
        try {
            //Initialize cURL.
            $ch = curl_init();

            //Set the URL that you want to GET by using the CURLOPT_URL option.
            curl_setopt($ch, CURLOPT_URL, Device::HOST . '/getgps/' . $uid);

            //Set CURLOPT_RETURNTRANSFER so that the content is returned as a variable.
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

            //Set CURLOPT_FOLLOWLOCATION to true to follow redirects.
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);

            //Execute the request.
            $data = curl_exec($ch);

            //Close the cURL handle.
            curl_close($ch);
        } catch (\Exception $e) {

        }

        return $data;
    }

    public function actionSetDataInterval($uid, $interval)
    {
        $data = null;
        try {
            //Initialize cURL.
            $ch = curl_init();

            //Set the URL that you want to GET by using the CURLOPT_URL option.
            curl_setopt($ch, CURLOPT_URL, Device::HOST . '/setdatainterval/' . $uid . '/' . $interval);

            //Set CURLOPT_RETURNTRANSFER so that the content is returned as a variable.
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

            //Set CURLOPT_FOLLOWLOCATION to true to follow redirects.
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);

            //Execute the request.
            $data = curl_exec($ch);

            //Close the cURL handle.
            curl_close($ch);
        } catch (\Exception $e) {
        }

        return $data;
    }

    public function actionStartDataAutoReport($uid)
    {
        $data = null;
        try {
            //Initialize cURL.
            $ch = curl_init();

            //Set the URL that you want to GET by using the CURLOPT_URL option.
            curl_setopt($ch, CURLOPT_URL, Device::HOST . '/startdataautoreport/' . $uid);

            //Set CURLOPT_RETURNTRANSFER so that the content is returned as a variable.
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

            //Set CURLOPT_FOLLOWLOCATION to true to follow redirects.
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);

            //Execute the request.
            $data = curl_exec($ch);

            //Close the cURL handle.
            curl_close($ch);
        } catch (\Exception $e) {

        }

        return $data;
    }

    public function actionStopDataAutoReport($uid)
    {
        $data = null;
        try {
            //Initialize cURL.
            $ch = curl_init();

            //Set the URL that you want to GET by using the CURLOPT_URL option.
            curl_setopt($ch, CURLOPT_URL, Device::HOST . '/stopdataautoreport/' . $uid);

            //Set CURLOPT_RETURNTRANSFER so that the content is returned as a variable.
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

            //Set CURLOPT_FOLLOWLOCATION to true to follow redirects.
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);

            //Execute the request.
            $data = curl_exec($ch);

            //Close the cURL handle.
            curl_close($ch);
        } catch (\Exception $e) {

        }

        return $data;
    }

    public function actionGetIntervalRequest($uid)
    {
        $data = null;
        try {
            //Initialize cURL.
            $ch = curl_init();

            //Set the URL that you want to GET by using the CURLOPT_URL option.
            curl_setopt($ch, CURLOPT_URL, Device::HOST . '/getintervalrequest/' . $uid);

            //Set CURLOPT_RETURNTRANSFER so that the content is returned as a variable.
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

            //Set CURLOPT_FOLLOWLOCATION to true to follow redirects.
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);

            //Execute the request.
            $data = curl_exec($ch);

            //Close the cURL handle.
            curl_close($ch);
        } catch (\Exception $e) {

        }

        return $data;
    }

}
