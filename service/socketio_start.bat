@echo off
cd /D %~dp0
echo AVLS is starting ...

node SocketIOServer\index.js

if errorlevel 255 goto finish
if errorlevel 1 goto error
goto finish

:error
echo.
echo AVLS konnte nicht gestartet werden
echo AVLS could not be started
pause

:finish
