from threading import Thread
from math import sin, cos, sqrt, atan2, radians
import time
import json
import serial
import DeviceModel
import TrackingModel

socketIO = None

# init serial port
SerialPort3 = None
SerialPort4 = None
SerialPort5 = None
SerialPort6 = None
SerialPort7 = None
sleep = 1

# Pause read serial, true = read, fase = pause read
running_com3 = True
running_com4 = True
running_com5 = True
running_com6 = True
running_com7 = True

# add description of car
cars = []


class Car:
    def __init__(self, uuid, lat, lng):
        self.uuid = uuid
        self.lat = lat
        self.lng = lng


def InitSerialCom3():
    global SerialPort3
    try:
        SerialPort3 = serial.Serial(
            port='com3',
            baudrate=9600,
            parity=serial.PARITY_NONE,
            stopbits=serial.STOPBITS_ONE,
            bytesize=serial.EIGHTBITS,
            timeout=1
        )

    except Exception as e:
        print(str(e))
        SerialPort3 = None


def InitSerialCom4():
    global SerialPort4
    try:
        SerialPort4 = serial.Serial(
            port='com4',
            baudrate=9600,
            parity=serial.PARITY_NONE,
            stopbits=serial.STOPBITS_ONE,
            bytesize=serial.EIGHTBITS,
            timeout=1
        )

    except Exception as e:
        print(str(e))
        SerialPort4 = None


def InitSerialCom5():
    global SerialPort5
    try:
        SerialPort5 = serial.Serial(
            port='COM5',
            baudrate=9600,
            parity=serial.PARITY_NONE,
            stopbits=serial.STOPBITS_ONE,
            bytesize=serial.EIGHTBITS,
            timeout=1
        )

    except Exception as e:
        print(str(e))
        SerialPort5 = None


def InitSerialCom6():
    global SerialPort6
    try:
        SerialPort6 = serial.Serial(
            port='com6',
            baudrate=9600,
            parity=serial.PARITY_NONE,
            stopbits=serial.STOPBITS_ONE,
            bytesize=serial.EIGHTBITS,
            timeout=1
        )

    except Exception as e:
        print(str(e))
        SerialPort6 = None


def InitSerialCom7():
    global SerialPort7
    try:
        SerialPort7 = serial.Serial(
            port='com7',
            baudrate=9600,
            parity=serial.PARITY_NONE,
            stopbits=serial.STOPBITS_ONE,
            bytesize=serial.EIGHTBITS,
            timeout=1
        )

    except Exception as e:
        print(str(e))
        SerialPort7 = None


def GetUID(src):
    try:
        inputs = src.split(",")
        if len(inputs) == 16:
            uid = inputs[12]
            return uid

        elif len(inputs) == 1:
            index_start = src.index("U")
            uid = src[index_start:index_start + 9]
            return uid
        else:
            return 0
    except Exception as e:
        print(str(e))
        print('error: src = ' + src)
        return 0


def GetStatus(src):
    try:
        if len(src) > 10:
            ln = len(src)
            status = src[ln - 7:ln - 3]
            if status == '1020':
                return 0
            else:
                return 1
        else:
            return 0

    except Exception as e:
        print(str(e))
        print('error: src = ' + src)
        return 0


def GetLatitude(src):
    try:
        inputs = str(src).split(",")
        for index, item in enumerate(inputs):
            if item == 'N':
                num = inputs[index - 1]
                latinputDegrees = int(float(num) / 100)
                latinputMinutes = (num.split(".")[0]).split(str(latinputDegrees))[1] + '.' + num.split(".")[1]
                latitude = latinputDegrees + (float(latinputMinutes) / 60)
                return latitude
    except Exception as e:
        print(str(e))
        print('error: src = ' + src)
        return 0


def GetLongitude(src):
    try:
        inputs = str(src).split(",")
        for index, item in enumerate(inputs):
            if item == 'E':
                num = inputs[index - 1]
                lnginputDegrees = int(float(num) / 100)
                lnginputMinutes = (num.split(".")[0]).split(str(lnginputDegrees))[1] + '.' + num.split(".")[1]
                longitude = lnginputDegrees + (float(lnginputMinutes) / 60)
                return longitude
    except Exception as e:
        print(str(e))
        print('error: src = ' + src)
        return 0


# convert string to hex
def toHex(s):
    lst = [int('0x02', 16)]
    for ch in s:
        hv = hex(ord(ch)).replace('0x', '')
        if len(hv) == 1:
            hv = '0' + hv
        lst.append(int('0x' + hv, 16))

    lst.append(int('0x03', 16))
    # return reduce(lambda x, y: x + y, lst)
    return lst


def WriteSerial(uid, cmd):
    device = DeviceModel.GetDevice(uid)
    comport = device['comport']

    sr = GetSerial(comport)

    SetRunning(sr, False)
    time.sleep(1)

    try:
        sr.write(serial.to_bytes(toHex(cmd)))
        time.sleep(1)
        return 'True'
    except Exception as e:
        print(str(e))
        print('error: cmd = ' + cmd)
        sr.close()
        return 'False'
    finally:
        SetRunning(sr, True)


def GetSerial(comport):
    global SerialPort3, SerialPort4, SerialPort5, SerialPort6, SerialPort7
    if comport == 3:
        return SerialPort3
    elif comport == 4:
        return SerialPort4
    elif comport == 5:
        return SerialPort5
    elif comport == 6:
        return SerialPort6
    else:
        return SerialPort7


def GetDistance(lat1, lng1, lat2, lng2):
    # approximate radius of earth in km
    R = 6373.0
    lon1, lat1, lon2, lat2 = map(radians, [lng1, lat1, lng2, lat2])
    dlon = lon2 - lon1
    dlat = lat2 - lat1

    a = sin(dlat / 2) ** 2 + cos(lat1) * cos(lat2) * sin(dlon / 2) ** 2
    c = 2 * atan2(sqrt(a), sqrt(1 - a))

    distance = R * c

    print("Result:", distance)

    return distance


def searchUUID(list, n):
    for i in range(len(list)):
        if list[i].uuid == n:
            return i
    return -1


def UpdateData(read, comport):
    global socketIO
    latitude = GetLatitude(read)
    longitude = GetLongitude(read)

    uid = GetUID(read)

    if uid != 0:
        DeviceModel.UpdateLastLogonDevice(uid, comport)
        if socketIO is not None:
            socketIO.emit("new message", json.JSONEncoder().encode({"device": uid, "last_on": DeviceModel.GetTime()}))

        if latitude == 0 or longitude == 0 or latitude is None or longitude is None:
            return 0

        # Todo check coordinates of two point
        index = searchUUID(cars, uid)
        if index >= 0:
            print("coordinates of two points:")
            last_lat = cars[index].lat
            last_lng = cars[index].lng

            d = GetDistance(latitude, longitude, last_lat, last_lng)
            if d > 0.12:
                cars.pop(index)
                cars.append(Car(uid, latitude, longitude))
                TrackingModel.InsertTrackingDB(uid, latitude, longitude)
        else:
            cars.append(Car(uid, latitude, longitude))
            TrackingModel.InsertTrackingDB(uid, latitude, longitude)

        # Todo print location
        print("UUID : %s", uid)
        print("slat : %s", latitude)
        print("slon : %s", longitude)

        if socketIO is not None:
            socketIO.emit("new message",
                          json.JSONEncoder().encode(
                              {"device": uid, "lat": latitude, "lng": longitude, "last_on": DeviceModel.GetTime()}))


def ReadSerialCom3():
    global running_com3
    while 1:
        try:
            if running_com3 and SerialPort3 is not None:
                read = str(SerialPort3.readline())
                if len(read) > 10:
                    print(read)
                    UpdateData(read, 3)

        except Exception as e:
            print(str(e))
            print('error')

        time.sleep(0.2)


def ReadSerialCom4():
    global running_com4
    while 1:
        try:
            if running_com4 and SerialPort4 is not None:
                read = str(SerialPort4.readline())
                if len(read) > 10:
                    print(read)
                    UpdateData(read, 4)

        except Exception as e:
            print(str(e))
            print('error')
        time.sleep(0.2)


def ReadSerialCom5():
    global running_com5
    while 1:
        try:
            if running_com5 and SerialPort5 is not None:
                read = str(SerialPort5.readline())
                if len(read) > 10:
                    UpdateData(read, 5)

        except Exception as e:
            print(str(e))
            print('error')
        time.sleep(0.2)


def ReadSerialCom6():
    global running_com6
    while 1:
        try:
            if running_com6 and SerialPort6 is not None:
                read = str(SerialPort6.readline())
                if len(read) > 10:
                    print(read)
                    UpdateData(read, 6)

        except Exception as e:
            print(str(e))
            print('error')
        time.sleep(0.2)


def ReadSerialCom7():
    global running_com7
    while 1:
        try:
            if running_com7 and SerialPort7 is not None:
                read = str(SerialPort7.readline())
                if len(read) > 10:
                    print(read)
                    UpdateData(read, 7)

        except Exception as e:
            print(str(e))
            print('error')
        time.sleep(0.2)


def SetRunning(sr, state=False):
    global running_com3
    global running_com4
    global running_com5
    global running_com6
    global running_com7

    try:
        if sr is SerialPort3:
            running_com3 = state

        elif sr is SerialPort4:
            running_com4 = state

        elif sr is SerialPort5:
            running_com5 = state

        elif sr is SerialPort6:
            running_com6 = state

        elif sr is SerialPort7:
            running_com7 = state
    except:
        print('error')


class SerialCom3Thread(Thread):
    def __init__(self, socketio):
        Thread.__init__(self)
        global socketIO
        socketIO = socketio

    def run(self):
        InitSerialCom3()
        ReadSerialCom3()
        return


class SerialCom4Thread(Thread):
    def __init__(self, socketio):
        Thread.__init__(self)
        global socketIO
        socketIO = socketio

    def run(self):
        InitSerialCom4()
        ReadSerialCom4()
        return


class SerialCom5Thread(Thread):
    def __init__(self, socketio):
        Thread.__init__(self)
        global socketIO
        socketIO = socketio

    def run(self):
        InitSerialCom5()
        ReadSerialCom5()
        return


class SerialCom6Thread(Thread):
    def __init__(self, socketio):
        Thread.__init__(self)
        global socketIO
        socketIO = socketio

    def run(self):
        InitSerialCom6()
        ReadSerialCom6()
        return


class SerialCom7Thread(Thread):
    def __init__(self, socketio):
        Thread.__init__(self)
        global socketIO
        socketIO = socketio

    def run(self):
        InitSerialCom7()
        ReadSerialCom7()
        return


def getSerial3Thread(socketio):
    return SerialCom3Thread(socketio)


def getSerial4Thread(socketio):
    return SerialCom4Thread(socketio)


def getSerial5Thread(socketio):
    return SerialCom5Thread(socketio)


def getSerial6Thread(socketio):
    return SerialCom6Thread(socketio)


def getSerial7Thread(socketio):
    return SerialCom7Thread(socketio)
