import DeviceModel
import SerialThread
import TrackingModel
import Service
import SocketIOServer
from socketIO_client import SocketIO, LoggingNamespace

socket_port = 8019

if __name__ == "__main__":
    socketIO = SocketIO(host='https://localhost', port=socket_port, verify=False)

    deviceModel = DeviceModel.Device()
    trackingModel = TrackingModel.getTrackingModel()

    service = Service.getService()
    service.daemon = False
    service.start()

    socketio_server = SocketIOServer.getService(socketIO)
    socketio_server.daemon = False
    socketio_server.start()

    SerialRead3 = SerialThread.getSerial3Thread(socketIO)
    SerialRead3.daemon = False

    SerialRead4 = SerialThread.getSerial4Thread(socketIO)
    SerialRead4.daemon = False

    SerialRead5 = SerialThread.getSerial5Thread(socketIO)
    SerialRead5.daemon = False

    SerialRead6 = SerialThread.getSerial6Thread(socketIO)
    SerialRead6.daemon = False

    SerialRead7 = SerialThread.getSerial7Thread(socketIO)
    SerialRead7.daemon = False

    SerialRead3.start()
    SerialRead4.start()
    SerialRead5.start()
    SerialRead6.start()
    SerialRead7.start()

    SerialRead3.join()
    SerialRead4.join()
    SerialRead5.join()
    SerialRead6.join()
    SerialRead7.join()

    print ('Server is running...')
