from threading import Thread
from threading import Lock
from flask import Flask
import DeviceModel
import SerialThread

app = Flask(__name__)
app_port = 8018


@app.route('/', methods=['POST', 'GET'])
def hello_world():
    return 'Server is running port ' + str(app_port)


@app.route('/getstatus/<uid>', methods=['POST', 'GET'])
def GetDeviceStatus(uid):
    try:
        cmd = 'xH' + uid + '24'
        return SerialThread.WriteSerial(uid, cmd)

    except Exception as e:
        print (str(e))
        print ('error: uid = ' + uid)
        return 'False'


@app.route('/getgps/<uid>', methods=['POST', 'GET'])
def GetDeviceGPS(uid):
    try:
        cmd = 'xR3' + uid + '24'
        return SerialThread.WriteSerial(uid, cmd)

    except Exception as e:
        print (str(e))
        print ('error: uid = ' + uid)
        return 'False'


@app.route('/setdatainterval/<uid>/<interval>', methods=['POST', 'GET'])
def SetDataInterval(uid, interval):
    try:
        cmd = 'xR7' + uid + "{0:0=4d}".format(int(interval)) + '{0:0=4d}'.format(int(interval)) + '001224'
        return SerialThread.WriteSerial(uid, cmd)

    except Exception as e:
        print (str(e))
        print('error: uid = ' + uid)
        return 'False'


@app.route('/startdataautoreport/<uid>', methods=['POST', 'GET'])
def StartDataAutoReport(uid):
    try:
        cmd = 'xR1' + uid + '24'
        return SerialThread.WriteSerial(uid, cmd)
    except Exception as e:
        print (str(e))
        print('error: uid = ' + uid)
        return 'False'


@app.route('/stopdataautoreport/<uid>', methods=['POST', 'GET'])
def StopDataAutoReport(uid):
    try:
        cmd = 'xR2' + uid + '24'
        return SerialThread.WriteSerial(uid, cmd)
    except Exception as e:
        print (str(e))
        print('error: uid = ' + uid)
        return 'False'


@app.route('/getintervalrequest/<uid>', methods=['POST', 'GET'])
def GetIntervalRequest(uid):
    try:
        cmd = 'xR6' + uid + '24'
        return SerialThread.WriteSerial(uid, cmd)
    except Exception as e:
        print (str(e))
        print('error: uid = ' + uid)
        return 'False'


class Service(Thread):
    def __init__(self):
        Thread.__init__(self)

    def run(self):
        global app
        app.run('0.0.0.0', app_port)


def getService():
    return Service()
