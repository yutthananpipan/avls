import mysql.connector
from time import time as t

host = 'localhost'
user = 'root'
port = 3306
password = ''
database = 'avls'


def GetDevice(uid):
    try:
        if uid != 0:
            cnx = mysql.connector.connect(host=host, user=user, passwd=password, database=database, port=port, auth_plugin='mysql_native_password')
            cursor = cnx.cursor()
            sql = "SELECT name, car_description, Lat, Lng, last_on, comport FROM device WHERE name like('" + uid + "')"
            cursor.execute(sql)
            data = cursor.fetchall()

            devices = []
            for item in data:
                i = {
                    'name': item[0],
                    'car_description': item[1],
                    'Lat': item[2],
                    'Lng': item[3],
                    'last_on': item[4],
                    'comport': item[5],
                }
                devices.append(i)
            cursor.close()
            cnx.close()

            return devices[0]
    except Exception as e:
        print (str(e))
        return None


def GetTime():
    # current date and time
    return int(t())


def UpdateLastLogonDevice(uid, comport):
    try:
        if uid != 0:
            cnx = mysql.connector.connect(host=host, user=user, passwd=password, database=database, port=port, auth_plugin='mysql_native_password')
            cursor = cnx.cursor()
            sql = "UPDATE device SET last_on =%s, comport = %s WHERE  name like('%s')"
            val = (GetTime(), comport, uid)
            cursor.execute(sql, val)

            cnx.commit()

            print(mycursor.rowcount, "record updated.")
            cursor.close()
            cnx.close()

    except Exception as e:
        print (str(e))
        print ('mysql error')


class DeviceModel:
    name = None  # UUID
    Lat = 0
    Lng = 0
    monitor = 0  # 0 = monitor, 1 = not monitor
    last_on = 0
    comport = 0  # number serial comport

    def __init__(self):
        print ('init')


def Device():
    return DeviceModel()
