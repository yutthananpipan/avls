@echo off
cd /D %~dp0
echo AVLS is starting ...

python3 Server.py

if errorlevel 255 goto finish
if errorlevel 1 goto error
goto finish

:error
echo.
echo AVLS konnte nicht gestartet werden
echo AVLS could not be started
pause

:finish
