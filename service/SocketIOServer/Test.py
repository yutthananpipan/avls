from math import sin, cos, sqrt, atan2, radians


class Car:
    def __init__(self, uuid, lat, lng):
        self.uuid = uuid
        self.lat = lat
        self.lng = lng


def GetDistance(lat1, lng1, lat2, lng2):
    # approximate radius of earth in km
    R = 6373.0
    lon1, lat1, lon2, lat2 = map(radians, [lng1, lat1, lng2, lat2])
    dlon = lon2 - lon1
    dlat = lat2 - lat1

    a = sin(dlat / 2) ** 2 + cos(lat1) * cos(lat2) * sin(dlon / 2) ** 2
    c = 2 * atan2(sqrt(a), sqrt(1 - a))

    distance = R * c

    print("Result:", distance)

    return distance


def searchUUID(list, n):
    for i in range(len(list)):
        if list[i].uuid == n:
            return i
    return -1


if __name__ == "__main__":
    d = GetDistance(13.781237, 100.575049, 13.781148, 100.575347)
    print ("distance: ", d)

    cars = []
    for i in range(1, 5):
        cars.append(Car('U0000' + str(i), i, i))

    index = searchUUID(cars, 'U00002')
    if index >= 0:
        print(cars[index].uuid)

    else:
        print("Not Found")
