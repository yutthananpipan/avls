import mysql.connector
from time import time as t

from DeviceModel import host, user, password, database, port

Id = 0
device_name = None
Lat = 0
Lng = 0


def GetTime():
    # current date and time
    return int(t())


def InsertTrackingDB(uid, latitude, longitude):
    try:
        if uid != 0:
            cnx = mysql.connector.connect(host=host, user=user, passwd=password, database=database, port=port, auth_plugin='mysql_native_password')
            cursor = cnx.cursor()

            # update Location to tracking table
            cursor.execute("INSERT INTO tracking (device_name, Lat, Lng, created_at) VALUES (%s, %s, %s, %s)",
                           (uid, latitude, longitude, GetTime()))

            # update Lat lng to Device table
            cursor.execute("UPDATE  device SET Lat = %s,  Lng  = %s WHERE name = %s", (latitude, longitude, uid))

            cnx.commit()

            print(cursor.rowcount, "record inserted.")
            cursor.close()
            cnx.close()

    except:
        print ('mysql error')


class TrackingModel:
    def __init__(self):
        print ('init')


def getTrackingModel():
    return TrackingModel()
