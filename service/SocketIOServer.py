from threading import Thread
from flask import Flask

SerialRead = None

app = Flask(__name__)
app_port = 8018


def on_connect():
    print('connect')
    # socketIO.emit('add user', 'python')


def on_disconnect():
    print('disconnect')


def on_reconnect():
    print('reconnect')


def on_joinuser(*args):
    print('on_joinuser', args)


def on_newmassage(*args):
    print('on_newmassage', args)


class SocketIOServer(Thread):
    def __init__(self, socketio):
        Thread.__init__(self)
        global SerialRead, socketIO
        socketIO = socketio

    def run(self):
        global app
        socketIO.on('connect', on_connect)
        socketIO.on('disconnect', on_disconnect)
        socketIO.on('reconnect', on_reconnect)

        # Listen
        socketIO.on('new message', on_newmassage)
        socketIO.on('user joined', on_joinuser)
        socketIO.wait()


def getService(socketio):
    return SocketIOServer(socketio)
