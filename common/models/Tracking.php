<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%tracking}}".
 *
 * @property int $id
 * @property string $device_name Device UUID
 * @property float|null $Lat
 * @property float|null $Lng
 * @property int|null $created_at
 * @property int|null $created_by
 *
 * @property Device $deviceName
 */
class Tracking extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%tracking}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['Lat', 'Lng'], 'number'],
            [['created_at', 'created_by'], 'integer'],
            [['device_name'], 'string', 'max' => 255],
            [['device_name', 'Lat', 'Lng'], 'unique', 'targetAttribute' => ['device_name', 'Lat', 'Lng']],
            [['device_name'], 'exist', 'skipOnError' => true, 'targetClass' => Device::className(), 'targetAttribute' => ['device_name' => 'name']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'device_name' => 'Device UUID',
            'Lat' => 'Lat',
            'Lng' => 'Lng',
            'created_at' => 'Created At',
            'created_by' => 'Created By',
        ];
    }

    /**
     * Gets query for [[DeviceName]].
     *
     * @return \yii\db\ActiveQuery|DeviceQuery
     */
    public function getDeviceName()
    {
        return $this->hasOne(Device::className(), ['name' => 'device_name']);
    }

    /**
     * {@inheritdoc}
     * @return TrackingQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new TrackingQuery(get_called_class());
    }
}
