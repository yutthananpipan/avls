<?php

namespace common\models;

/**
 * This is the ActiveQuery class for [[Temples]].
 *
 * @see Temples
 */
class TemplesQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return Temples[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return Temples|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
