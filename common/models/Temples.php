<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "temples".
 *
 * @property int $t_id
 * @property string $t_name ชื่อวัด
 * @property string $t_sect
 * @property string $t_num เลขที่
 * @property string|null $t_moo หมู่ที่
 * @property string|null $t_soi ซอย
 * @property string|null $t_road ถนน
 * @property int|null $district ตำบล
 * @property int $amphur
 * @property int $province
 * @property string $geography
 * @property string $budd_geo
 * @property string|null $zipcode รหัสไปรษณีย์
 * @property string|null $t_phone1 เบอร์โทรวัด1
 * @property string|null $t_phone2 เบอร์โทรวัด2
 * @property string|null $fax แฟกส์
 * @property string|null $website website
 * @property string|null $email email
 * @property string|null $googleMap googlemap
 */
class Temples extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'temples';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['t_name', 't_sect', 't_num', 'geography', 'budd_geo'], 'required'],
            [['district', 'amphur', 'province'], 'integer'],
            [['t_name', 't_num', 't_moo', 't_soi', 't_road', 'zipcode', 't_phone1', 't_phone2', 'fax', 'website', 'email'], 'string', 'max' => 50],
            [['t_sect', 'geography'], 'string', 'max' => 220],
            [['budd_geo'], 'string', 'max' => 10],
            [['googleMap'], 'string', 'max' => 250],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            't_id' => 'T ID',
            't_name' => 'ชื่อวัด',
            't_sect' => 'T Sect',
            't_num' => 'เลขที่',
            't_moo' => 'หมู่ที่',
            't_soi' => 'ซอย',
            't_road' => 'ถนน',
            'district' => 'ตำบล',
            'amphur' => 'Amphur',
            'province' => 'Province',
            'geography' => 'Geography',
            'budd_geo' => 'Budd Geo',
            'zipcode' => 'รหัสไปรษณีย์',
            't_phone1' => 'เบอร์โทรวัด1',
            't_phone2' => 'เบอร์โทรวัด2',
            'fax' => 'แฟกส์',
            'website' => 'website',
            'email' => 'email',
            'googleMap' => 'googlemap',
        ];
    }

    /**
     * {@inheritdoc}
     * @return TemplesQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new TemplesQuery(get_called_class());
    }
}
