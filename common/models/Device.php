<?php

namespace common\models;

use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

/**
 * This is the model class for table "{{%device}}".
 *
 * @property string $name
 * @property int $status_id
 * @property int $area_id
 * @property int $update_interval ช่วงเวลาดึงข้อมูล (second)
 * @property string|null $car_description ป้ายทะเบียนรถยนต์
 * @property float|null $Lat Latitude
 * @property float|null $Lng Longitude
 * @property int $monitor 0 : do not monitor, 1: monitor
 * @property int|null $created_at เวลาที่สร้างข้อมูล
 * @property int|null $created_by สร้างข้อมูลโดย
 * @property int|null $updated_at เวลาที่อัพเดทข้อมูล
 * @property int|null $updated_by อัพเดทข้อมูลโดย
 * @property int|null $last_on เวลาที่ใช้งานล่าสุด
 * @property int|null $comport Number of comport on computer ex. 3
 * @property int|null $update_request
 *
 * @property User $createdBy
 * @property User $updatedBy
 * @property Tracking[] $trackings
 */
class Device extends \yii\db\ActiveRecord
{
    const HOST = 'http://localhost:8018';

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%device}}';
    }

    public function behaviors()
    {
        return [
            'blameable' => [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
            ],

        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'update_interval', 'comport', 'monitor'], 'required'],
            [['status_id', 'area_id', 'update_interval', 'monitor', 'created_at', 'created_by', 'updated_at', 'updated_by', 'last_on', 'comport', 'update_request'], 'integer'],
            [['Lat', 'Lng'], 'number'],
            [['update_interval'], 'integer', 'min' => 60, 'max' => 3600],
            [['name', 'car_description'], 'string', 'max' => 255],
            [['name'], 'unique'],
            [['created_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['created_by' => 'id']],
            [['updated_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['updated_by' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'name' => 'รหัสวิทยุ',
            'status_id' => 'Status ID',
            'area_id' => 'Area ID',
            'update_interval' => 'ช่วงเวลาดึงข้อมูล (วินาที)',
            'car_description' => 'ป้ายทะเบียนรถยนต์',
            'Lat' => 'Latitude ',
            'Lng' => 'Longitude',
            'monitor' => 'การส่งข้อมูลแบบอัตโนมัติ',
            'created_at' => 'เวลาที่สร้างข้อมูล',
            'created_by' => 'สร้างข้อมูลโดย',
            'updated_at' => 'เวลาที่อัพเดทข้อมูล',
            'updated_by' => 'อัพเดทข้อมูลโดย',
            'last_on' => 'เวลาที่ใช้งานล่าสุด',
            'comport' => 'คอมพอร์ทของพื้นที่ใช้งาน',
        ];
    }

    /**
     * Gets query for [[CreatedBy]].
     *
     * @return \yii\db\ActiveQuery|yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    /**
     * Gets query for [[UpdatedBy]].
     *
     * @return \yii\db\ActiveQuery|yii\db\ActiveQuery
     */
    public function getUpdatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'updated_by']);
    }

    /**
     * Gets query for [[Trackings]].
     *
     * @return \yii\db\ActiveQuery|TrackingQuery
     */
    public function getTrackings()
    {
        return $this->hasMany(Tracking::className(), ['device_name' => 'name']);
    }

    /**
     * {@inheritdoc}
     * @return DeviceQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new DeviceQuery(get_called_class());
    }
}
