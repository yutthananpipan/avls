<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "area".
 *
 * @property int $id
 * @property string $name
 * @property string|null $Address1 บ้านเลขที่ ซอย ถนน
 * @property int|null $temple_id
 * @property int|null $districts_id
 * @property int|null $province_id
 * @property int|null $amphur_id
 * @property int|null $created_at
 * @property int|null $created_by
 * @property int|null $updated_at
 * @property int|null $updated_by
 *
 * @property Device[] $devices
 */
class Area extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'area';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['Address1'], 'string'],
            [['temple_id', 'districts_id', 'province_id', 'amphur_id', 'created_at', 'created_by', 'updated_at', 'updated_by'], 'integer'],
            [['name'], 'string', 'max' => 255],
            [['name'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'Address1' => 'บ้านเลขที่ ซอย ถนน',
            'temple_id' => 'Temple ID',
            'districts_id' => 'Districts ID',
            'province_id' => 'Province ID',
            'amphur_id' => 'Amphur ID',
            'created_at' => 'Created At',
            'created_by' => 'Created By',
            'updated_at' => 'Updated At',
            'updated_by' => 'Updated By',
        ];
    }

    /**
     * Gets query for [[Devices]].
     *
     * @return \yii\db\ActiveQuery|DeviceQuery
     */
    public function getDevices()
    {
        return $this->hasMany(Device::className(), ['area_id' => 'id']);
    }

    /**
     * {@inheritdoc}
     * @return AreaQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new AreaQuery(get_called_class());
    }

    public static function getAreas()
    {
        $area = array();
        $area[3] = 'พัทยา';
        $area[4] = 'ระยอง';
        $area[5] = 'จันทบุรี';
        $area[6] = 'ชลบุรี';
        $area[7] = 'ฉะเชิงเทรา';

        return $area;
    }

    public static function getAreaComport($name)
    {
        $area = array();
        $area['Pattaya'] = 3;
        $area['Rayong'] = 4;
        $area['Chanthaburi'] = 5;
        $area['Chonburi'] = 6;
        $area['Chachoengsao'] = 7;

        return $area[$name];
    }

    public static function getAreaName($comport)
    {
        $area = array();
        $area[3] = 'Pattaya';
        $area[4] = 'Rayong';
        $area[5] = 'Chanthaburi';
        $area[6] = 'Chonburi';
        $area[7] = 'Chachoengsao';

        return $area[$comport];
    }
}
