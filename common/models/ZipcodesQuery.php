<?php

namespace common\models;

/**
 * This is the ActiveQuery class for [[Zipcodes]].
 *
 * @see Zipcodes
 */
class ZipcodesQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return Zipcodes[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return Zipcodes|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
